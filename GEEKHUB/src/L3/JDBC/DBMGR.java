package L3.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBMGR {

	private static String status = "";
	private static boolean connectedToDb = false;
	private static Connection conn;

	// status guys=)
	public static String getstatus() {
		return status;
	}

	public static boolean connectedToDb() {
		return connectedToDb;
	}

	// host,login,pwd, DBname and connect
	public static Connection getConnection(String url, String dbname,
			String usr, String pass) throws SQLException,
			ClassNotFoundException {
		if (connectedToDb)
			stopConnection();
		Class.forName("com.mysql.jdbc.Driver");
		try {
			status = "/////Connecting to DB";
			Show(status);
			conn = DriverManager.getConnection(url + dbname, usr, pass);
			connectedToDb = true;
			status = "Connected";
			Show(status);
			View.Blank();
			View.askNew();
			return conn;
		} catch (Exception e) {
			View.Blank();
			status = "/////Failed to Connect";
			Show(status);
		}
		View.Blank();
		View.askNew();
		return null;

	}

	public static void stopConnection() throws SQLException {
		View.Blank();
		status = "Closing DB";
		if (connectedToDb) {
			try {
				conn.close();
				status = "DB closed";
				connectedToDb = false;
			} catch (Exception E) {
				status = "ERROR occured";
			} finally {
				Show(status);
			}
		} else {
			status = "Nothing to close";
			Show(status);
		}

	}

	// DBView, must show result and count

	public static void sendCommand(String cmd) throws SQLException,
			ClassNotFoundException {

		// Viewing help
		if (cmd.toLowerCase().equals("help")) {
			View.Blank();
			help();
			View.Blank();
		}
		// Check commands to Connect/Disconnect from DB.
		else if (cmd.toLowerCase().equals("connect"))
			View.askAtStart();
		else if (cmd.toLowerCase().equals("disconnect"))
			stopConnection();

		else if (cmd.toLowerCase().equals("exit"))
			System.exit(0);
		// If need: View.askAtStart();
		else {

			View.Blank();
			Show("Command:  " + cmd);
			Statement statement = conn.createStatement();
			try {
				if (cmd.contains("DELETE") || cmd.contains("INSERT")
						|| cmd.contains("UPDATE")) {
					int rowsChanged = statement.executeUpdate(cmd);
					status = "Updated: " + rowsChanged;
					Show(status);
				} else {
					ResultSet result = statement.executeQuery(cmd);
					viewTable(result);

				}
			} catch (Exception e) {
				status = "ERROR: check command.";
				Show(status);
			}
		}
		View.Blank();
		View.askNew();
	}

	private static void viewTable(ResultSet RS) throws SQLException {
		View.Blank();
		// Viewing heading raw
		int numColumns = RS.getMetaData().getColumnCount();
		int rawsCount = 0;
		int[] positionToStart = new int[numColumns + 1];
		positionToStart[0] = 0;
		String namesRaw = "";
		String current = "";
		String separator = "";
		int l;
		int i = 1;
		// for (int i=1; i<=(numColumns); i++){
		while (i <= numColumns) {
			if (RS.getMetaData().getColumnDisplaySize(i) > RS.getMetaData()
					.getColumnName(i).length())
				l = RS.getMetaData().getColumnDisplaySize(i);
			else
				l = RS.getMetaData().getColumnName(i).length();
			current = RS.getMetaData().getColumnName(i);
			while (current.length() < l)
				current += " ";
			namesRaw += current + "|";
			positionToStart[i] = namesRaw.length();
			i += 1;
		}
		// Making separator
		for (int is = 0; is < namesRaw.length(); is++)
			separator += "-";
		Show(separator);
		Show(namesRaw);
		Show(separator);
		// Heading raw is now visible
		// Viewing Data Raws
		current = "";
		try {
			while (RS.next()) {
				rawsCount++;
				for (int ic = 1; ic <= numColumns; ic++) {
					current += RS.getObject(ic);
					while (current.length() < ((positionToStart[ic] - 1)))
						current += " ";
					current += "|";
				}
				Show(current);
				current = "";
			}
			// Finishing table
			Show(separator);
			status = rawsCount + " records listed";
			Show(status);
		} finally {
		}
		;
	}

	private static void help() {
		Show("command list:");
		Show("connect, disconnect, <SQL command>, exit");
	}

	public static void Show(String txt) {
		System.out.println(txt);
	}

	public static void ShowH(String txt) {
		System.out.print(txt);
	}

}
