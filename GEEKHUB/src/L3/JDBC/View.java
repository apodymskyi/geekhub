package L3.JDBC;

import java.sql.SQLException;
import java.util.Scanner;

public class View {

	public static void main(String args[]) throws SQLException,
			ClassNotFoundException {
		// DBMGR.getConnection("jdbc:mysql://localhost:3306/", "employees",
		// "root", "1");
		// askNew();
		// DBMGR.sendCommand("SELECT * FROM employees");
		askAtStart();
	}

	public static void askAtStart() throws SQLException, ClassNotFoundException {
		String url = ask("Please input Url to DB=");
		String dbname = ask("Input DB Name=");
		String usr = ask("Input Username in DB=");
		String pass = ask("Input pass for " + usr + ":");
		DBMGR.getConnection(url, dbname, usr, pass);
	}

	public static void askNew() throws SQLException, ClassNotFoundException {
		DBMGR.sendCommand(ask("Input new command:"));
	}

	public static String ask(String aWhat) {
		System.out.print(aWhat);
		Scanner in = new Scanner(System.in);
		System.out.println();
		return in.nextLine();
	}

	public static void Blank() {
		System.out.println("--------------------------");
	}

}
