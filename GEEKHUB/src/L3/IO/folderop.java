package L3.IO;

import java.io.File;
import java.util.Scanner;

public class folderop {

	private static String path = "";
	private static int cmd = -1;
	private static boolean status = false;

	/*
	 * Input syntaxis: [Path] [action_code] [arg0] [arg1]
	 * 
	 * action codes = 0 to delete, 1 to create, 2 to rename
	 * 
	 * arg0 dir name;
	 * arg1 is used to specify new name for directory;
	 */
	
	public static void main(String args[]) {
		if (checkcmd(args))
			doCmd(args);
		else
			status=false;
		show(""+status);
	}

	public static boolean checkcmd(String args[]) {
		if (args.length < 3) {
			
		}
		try {
			cmd = Integer.parseInt(args[1]);
			if (!(cmd == 0 || cmd == 1 || cmd == 2)) {
				show("Wrong action_code");
				return false;
			}
			if ((cmd == 2)&&(args.length<4)){
				show("Not enough argz");
				return false;
			}
		} catch (Exception e) {
			show("Wrong action_code");
			return false;
		}
		return true;

	}

	public static boolean doCmd(String args[]) {
		if (checkpath(args[0])) {
			switch (cmd) {
			case 0:
				delete(args[2]);
				break;
			case 1:
				create(args[2]);
				break;
			case 2:
				rename(args[2], args[3]);
				break;
			}
			return status;
		} else
			return false;

	}

	public static boolean checkpath(String pathTry) {
		try {
			File dir = new File(pathTry);
			if (dir.isDirectory()) {
				path = pathTry;
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			show("Wrong path name");
			return false;
		}

	}

	public static void create(String name) {
		File dir = new File(path);
		if  (new File(dir.getAbsolutePath() + "\\" + name).mkdir()){
			show ("Directory succesfully created");
			status=true;
		}	else {
			show ("Directory NOT created");
			status=false;
		}

	}

	public static void delete(String name) {
		File dir = new File(path);
		if (dir.isDirectory())
			if (new File(dir.getAbsolutePath() + "\\" + name).delete()){
				show ("Directory was Deleted");
				status=true;
			}
		else{
			show ("Directory was NOT Deleted");
			status=false;
		}
	}

	public static void rename(String name, String newName) {
		try {
			File dir = new File(path);
			File old = new File(dir.getAbsolutePath() + "\\" + name);
			if (old.isDirectory()){
				if (old.renameTo(new File(dir.getAbsolutePath() + "\\"+ newName))){
					show("Dir is Renamed now");
					status=true;
				}
			}else{
				show("Unable to rename: no target dir, or is not a dir");
				status=false;
			}
		} catch (Exception e) {
			show("Wrong dir to rename or move, new name maybe wrong too");
			status=false;
		}
	}

	public static String ask(String aWhat) {
		System.out.print(aWhat);
		Scanner in = new Scanner(System.in);
		System.out.println();
		return in.nextLine();
	}

	public static void show(String aWhat) {
		System.out.println(aWhat);
	}
}
