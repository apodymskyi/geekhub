package L3.IO;

import java.io.*;
import java.util.Scanner;

public class copyfiles {

	private static String path = "D:\\";
	private static String aim = "D:\\";

	public static void main(String args[]) throws Exception {
		path = ask("Type path:");
		pickfile();
		copyfile();
	}

	public static void pickfile() {
		try {
			File dir = new File(path);
			if (dir.isDirectory()) {
				show("U can copy this files:");
				for (File file : dir.listFiles())
					show(file.getName());
				aim += ask("Enter the file name you want to copy:");
				File aimF = new File(aim);
				if (aimF.isFile())
					return;
				else {
					aim = path;
					show("Wrong file name! Try again");
					pickfile();
				}
			} else {
				show("Wrong Dir selected");
				path = ask("Type other path:");
				pickfile();
			}
		} catch (Exception e) {
			show("Wrong source file or main directory is missing");
			pickfile();
		}
	}

	public static void copyfile() throws IOException {
		File aimF = new File(aim);
		File aimFcopied = new File(aim + "2");
		String ans = ask("Will copy " + aim + ", use buffer (Y/N):");
		if (ans.toLowerCase().equals("y")) {
			long time = System.currentTimeMillis();
			BufferedInputStream inB = new BufferedInputStream(
					new FileInputStream(aim));
			BufferedOutputStream outB = new BufferedOutputStream(
					new FileOutputStream(aimFcopied));
			while (true) {
				int data = inB.read();
				if (data == -1)
					break;
				outB.write(data);
			}
			inB.close();
			outB.close();
			time -= System.currentTimeMillis();
			time = Math.abs(time);
			show("Time elapsed " + time);
		} else {

			long time = System.currentTimeMillis();
			InputStream in = new FileInputStream(aimF);
			OutputStream out = new FileOutputStream(aimFcopied);
			byte[] buf = new byte[1];
			int len;
			while ((len = in.read(buf)) > 0)
				out.write(buf, 0, len);
			in.close();
			out.close();
			show("File copied.");
			time -= System.currentTimeMillis();
			time = Math.abs(time);
			show("Time elapsed " + time);
		}
	}

	public static String ask(String aWhat) {
		System.out.print(aWhat);
		Scanner in = new Scanner(System.in);
		System.out.println();
		return in.next();
	}

	public static void show(String aWhat) {
		System.out.println(aWhat);
	}
}
