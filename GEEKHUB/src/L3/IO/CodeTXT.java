package L3.IO;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CodeTXT {

	private static List<String> codepage = new ArrayList<String>();

	private static void fillList() {
		codepage.add("Windows-1251");
		codepage.add("KOI-8R");
		codepage.add("KOI-8U");
		codepage.add("ISO-8859-5");
		codepage.add("UTF-8");
		codepage.add("Cp1251");
	}

	public static void main(String args[]) throws IOException {

		// Console asks codepage from list
		fillList();
		System.out.println("Supported codepages:");
		int i = 0;
		for (String s : codepage) {
			System.out.println(i + " " + s);
			i++;
		}
		// Trying to type index

		int indx = getIndex("original", i);
		int outdx = getIndex("new", i);

		// Encode/decode txt

		String workingDir = System.getProperty("user.dir");
		// Suppose to work in WIN)))
		String a = "";
		a += (char) 40;
		String b = "";
		b += (char) 92;
		b += (char) 92;
		workingDir.replaceAll(a, b);
		workingDir += "\\";
		System.out.println(workingDir);
		try {
			Reader reader = new InputStreamReader(new FileInputStream(
					workingDir + "input.txt"), codepage.get(indx));
			Writer writer = new OutputStreamWriter(new FileOutputStream(
					workingDir + "output.txt"), codepage.get(outdx));
			int c = 0;
			while ((c = reader.read()) >= 0) {
				writer.write(Character.toUpperCase((char) c));
			}
			reader.close();
			writer.close();
		} catch (IOException e) {
			System.out.println("I or O file not  found");
		}
	}

	public static int getIndex(String aWhich, int i) {
		// i is a total amount of i-xses
		int indx = 0;
		try {
			indx = askInt("Type " + aWhich
					+ "  file codepage index from list above:");
			if ((indx < 0) && (indx > i))
				indx = askInt("Type "
						+ aWhich
						+ " file codepage index from list above (index from 0 to"
						+ i + " :");
			return indx;
		} catch (Exception e) {
			System.out.println("Not an integer found");
			return indx;
		}

	}

	public static int askInt(String aWhat) {
		System.out.print(aWhat);
		Scanner in = new Scanner(System.in);
		System.out.println();
		return in.nextInt();
	}

	public static String ask(String aWhat) {
		System.out.print(aWhat);
		Scanner in = new Scanner(System.in);
		System.out.println();
		return in.next();

	}
}
