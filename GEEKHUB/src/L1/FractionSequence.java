package L1;

public class FractionSequence {

	public static void main(String args[]) {

		int n = 0;
		try {
			n = Integer.parseInt(args[0]);
		} catch (Exception error) {
			System.out.println("Wrong parameter");
		}
		FractionSequenceOp(n);
	}

	public static void FractionSequenceOp(int n) {
		if (n != 0) {
			for (float i = 1; i <= n; i++)
				System.out.print((1 / i) + " ");
		} else
			System.out.print("-");
	}

}
