package L1;

public class Sum {
	public static void main(String args[]) {

		// Check two arguments
		if (args.length >= 2) {
			// Assign arguments
			String m = args[0];
			String n = args[1];
			// Check type and Output sum
			Summ(m, Check(m), n, Check(n));
		} else
			System.out.println("-");
	}

	public static String Check(String in) {
		try {
			Integer.parseInt(in);
			if ((in.substring(0, 1).equals("0"))
					&& (!(in.substring(1, 2).equals("x"))))
				return "Octa";
			else
				return "Int";
		} catch (Exception error) {
			try {
				Float.parseFloat(in);

				return "Float";
			} catch (Exception errorr) {
				return "-";
			}
		}
	}

	public static void Summ(String m, String mtype, String n, String ntype) {
		if (mtype.equals("Octa")) {
			mtype = "Float";
			m = MakeFloat(m);
		}
		if (ntype.equals("Octa")) {
			ntype = "Float";
			n = MakeFloat(n);
		}

		if (mtype.equals("-") || ntype.equals("-"))
			System.out.println("-");

		else if (mtype.equals("Int") && ntype.equals("Int"))
			System.out.println(Integer.parseInt(m) + Integer.parseInt(n));

		else if (mtype.equals("Int") && ntype.equals("Float"))
			System.out.println((float) Integer.parseInt(m)
					+ Float.parseFloat(n));

		else if (mtype.equals("Float") && ntype.equals("Int"))
			System.out.println(Float.parseFloat(m)
					+ (float) Integer.parseInt(n));

		else if (mtype.equals("Float") && ntype.equals("Float"))
			System.out.println(Float.parseFloat(m) + Float.parseFloat(n));

	}

	public static String MakeFloat(String in) {
		int length = in.length() - 1;
		double n = Integer.parseInt(in);
		double a = 0;
		for (int i = 0; i < length; i++) {
			a += (n % 10) * Math.pow(8, i);
			n = (int) (n / 10);
		}
		return Double.toString(a);

	}
}
