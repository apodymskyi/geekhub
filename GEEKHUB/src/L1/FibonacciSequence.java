package L1;

public class FibonacciSequence {

	public static void main(String args[]) {

		int n = 0;
		System.out.println(args[0]);
		try {
			n = Integer.parseInt(args[0]);
		} catch (Exception error) {
			System.out.println("Wrong parameter");
		}
		FibonacciSequenceOp(n);
	}

	public static void FibonacciSequenceOp(int n) {
		double current = 0;
		double pre1 = 0;
		double pre2 = 1;
		switch (n) {
		case 0:
			System.out.print("-");
			break;
		case 1:
			System.out.print(pre1);
			break;
		case 2:
			System.out.print(pre1 + " " + pre2);
			break;
		default: {
			System.out.print(pre1 + " ");
			System.out.print(pre2 + " ");
			for (int i = 2; i < n; i++) {
				current = pre2 + pre1;
				System.out.print(current + " ");
				pre1 = pre2;
				pre2 = current;
			}
		}
		}
	}
}
