package L1;

public class CharSequence {

	public static void main(String args[]) {

		int m = 0;
		int n = 0;
		try {
			m = Integer.parseInt(args[0]);
			n = Integer.parseInt(args[1]);
			if (m > n) {
				int t = n;
				n = m;
				m = t;
			}
		} catch (Exception error) {
			System.out.println("Wrong parameter");
		}
		CharSequenceOp(m, n);
	}

	public static void CharSequenceOp(int m, int n) {
		for (int i = m; i <= n; i++)
			System.out.print((char) i + "  ");
	}
}
