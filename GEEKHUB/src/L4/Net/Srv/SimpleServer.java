package L4.Net.Srv;

import java.io.*;
import java.net.*;

public class SimpleServer {

	public static void main(String args[]) throws IOException {
		Start: show("Server started");
		ServerSocket serverSocket = new ServerSocket(23);
		Socket socket = serverSocket.accept();

		InputStream is = socket.getInputStream();
		OutputStream os = socket.getOutputStream();

		DataInputStream in = new DataInputStream(is);
		DataOutputStream out = new DataOutputStream(os);

		String line = null;
		System.out.println("IN:");
		try {
			while (true) {
				line = in.readUTF();
				show(line);
				if (line.length() == 0)
					line = " ";
				line = Character.toString(line.charAt(0));
				out.writeUTF(line);
				out.flush();
			}
		} catch (SocketException socketEcxeption) {
			show("Lost the client");
		}
	}

	public static void show(String aWhat) {
		System.out.println(aWhat);
	}
}
