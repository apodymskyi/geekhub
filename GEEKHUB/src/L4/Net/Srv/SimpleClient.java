package L4.Net.Srv;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class SimpleClient {

	public static void main(String args[]) throws UnknownHostException,
			IOException {
		try {
			Socket socket = new Socket("localhost", 23);
			show("Connection:" + socket.isConnected());
			OutputStream sout = socket.getOutputStream();
			InputStream sin = socket.getInputStream();
			DataInputStream in = new DataInputStream(sin);
			DataOutputStream out = new DataOutputStream(sout);

			BufferedReader keyboard = new BufferedReader(new InputStreamReader(
					System.in));
			String line = null;
			show("Sending by Enter");
			System.out.print("Send:");

			while (true) {
				line = keyboard.readLine();
				out.writeUTF(line);
				out.flush();
				if (line.length() == 0)
					line = " ";
				line = in.readUTF();
				show("Response: " + line);
				show("");
			}
		} catch (ConnectException e) {
			show("Could not connect");

		}
	}

	public static void show(String aWhat) {
		System.out.println(aWhat);
	}

}
