package L4.Net.Link;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Link {

	private static List<String> all = new ArrayList<String>();
	private static List<String> code2xx = new ArrayList<String>();
	private static List<String> code3xx = new ArrayList<String>();
	private static List<String> code4xx = new ArrayList<String>();

	public static void main(String args[]) throws MalformedURLException {
		if ((args.length != 0) && (args[0].length() != 0))
			askParsing(args[0]);
		else {
			// askParsing();
			System.exit(-1);
		}

	}

	public static void parseLinks(URL url) {
		String url1 = url.toString();
		try {
			URLConnection conn = new URL(url1).openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String htmlcode = "";
			String str;
			// Retrieveing html-code
			while ((str = in.readLine()) != null)
				htmlcode += str;
			in.close();
			// Html-code retrieved

			// Looking for links
			int startL = 0;
			int endL = 0;
			String fS = "href=";
			String leftEnd = "\"";
			while (true) {
				startL = htmlcode.indexOf(fS, endL);
				if (startL == -1)
					break;
				endL = htmlcode.indexOf(leftEnd, startL + fS.length() + 1);
				all.add(htmlcode.substring(startL + fS.length() + 1, endL));
			}
			// // Show all found uncheked links
			// System.out.println("Found:");
			// for (String s : all)
			// System.out.println(s);
			// System.out.println("------------------------------");
			responseC(all);
		} catch (IOException e) {
		}

	}

	public static void responseC(List<String> all) throws IOException {
		Iterator<String> it = all.iterator();
		int code = 0;
		String u1 = "";
		while (it.hasNext()) {
			try {
				u1 = (String) it.next();
				code = 0;
				URL url = new URL(u1);
				try {
					HttpURLConnection conn = (HttpURLConnection) url
							.openConnection();
					conn.setRequestMethod("HEAD");
					code = conn.getResponseCode();
					// System.out.println(url + " code " + code);
					if ((code >= 100) && (code < 300))
						code2xx.add(u1);
					else if ((code >= 300) && (code < 400))
						code3xx.add(u1);
					else {
						code4xx.add(u1);
					}
					it.remove();
				} catch (IOException e) {
					code = 404;
					code4xx.add(u1);
					it.remove();
				}
			} catch (Exception e) {
				// System.out.println((String) it.next() + " #code " + code);
				code4xx.add(u1);
				it.remove();
			}

		}
		all.clear(); // for insurance
		// System.out.println("all size= " + all.size());
		// System.out.println("l2size= " + l2.size());
		// System.out.println("l3size= " + l3.size());
		// System.out.println("lxsize= " + lx.size());
		show(1);
	}

	public static void askParsing() {
		String u1 = ask("Url=");
		try {
			URL url = new URL(u1);
			System.out.println("Try to parse: " + u1);
			parseLinks(url);
		} catch (IOException e) {
			System.out.println("Not an URl");
			askParsing();
		}
	}

	public static void askParsing(String aUrl) {
		try {
			URL url = new URL(aUrl);
			System.out.println("Try to parse: " + aUrl);
			parseLinks(url);
		} catch (IOException e) {
			System.out.println("Not an URl");
			askParsing();
		}
	}

	public static void show(int i) {
		if (i == 0) {
			System.out.println("HTTP 2xx links: " + code2xx.size());
			System.out.println("HTTP 3xx links: " + code3xx.size());
			System.out.println("HTTP 4xx-5xx links: " + code4xx.size());
		} else {
			System.out.println("HTTP 2xx links:");
			for (String s : code2xx)
				System.out.println(s);
			System.out.println("---------------");
			System.out.println("HTTP 3xx links:");
			for (String s : code3xx)
				System.out.println(s);
			System.out.println("---------------");
			System.out.println("HTTP 4xx-5xx links:");
			for (String s : code4xx)
				System.out.println(s);
			System.out.println("---------------");
		}
	}

	public static String ask(String aWhat) {
		System.out.print(aWhat);
		Scanner in = new Scanner(System.in);
		return in.next();
	}
}
