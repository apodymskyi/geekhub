package L4.Threads;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Launcher {

	private static String dir_name = "d:\\music\\";
	private static String filter = ".mp3";
	public static List<File> all = new ArrayList<File>();
	public static List<File> found = new ArrayList<File>();

	public static void main(String args[]) throws IOException {
		long startTime = System.currentTimeMillis();
		File dir = new File(dir_name);
		for (File file : dir.listFiles())
			all.add(file);
		Thread1.show(all, found, filter);
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		for (File file : found) {
			System.out.println(file.getName());
		}
		System.out.println("TotalTime: " + totalTime);
		System.out.println("TotalCount:" + found.size());

	}

}
