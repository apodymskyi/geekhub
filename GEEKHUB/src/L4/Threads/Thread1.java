package L4.Threads;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Thread1 implements Runnable {

	public void run() {

	}

	public synchronized static void show(List<File> listTodo, List<File> found,
			String filter) throws IOException {
		int fl = filter.length();
		// ListIterator<File> i = listTodo.listIterator();
		while (listTodo.size() > 0) {
			File c = listTodo.get(0);
			if (c.isDirectory()) {
				for (File file : c.listFiles()) {
					listTodo.add(file);
				}
			} else {
				String name = c.getName().toLowerCase();
				int l = name.length();
				if (l > fl)
					if (name.substring(l - fl).equals(filter)) {
						found.add(c);
					}
			}
			listTodo.remove(0);
		}
	}

}
