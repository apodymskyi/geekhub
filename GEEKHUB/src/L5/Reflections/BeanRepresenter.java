package L5.Reflections;

import java.lang.reflect.*;

import L2.Garage.Car;
import L2.Lang.Cat;

public class BeanRepresenter {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException {
		Cat Simon = new Cat(5, 1, 2, 3);
		int rgbc[] = { 1, 2, 3 };
		Cat Simon2 = new Cat(5, rgbc);
		Car C1 = new Car("Kalina", "red", 10);
		toRepresent(Simon);
		toRepresent(C1);
		toRepresent(Simon2);
	}
		
	public static void toRepresent(Object obj) throws IllegalArgumentException, IllegalAccessException{
		Class aClass = obj.getClass();
		showln("*"+aClass.getSuperclass().getSimpleName());
		showln("|");
		showln("+-+"+aClass.getSimpleName());
		Field[] fields = aClass.getDeclaredFields(); //All fields 
		for(Field fld: fields){
			showln("  |");
			show("  ++-");
			fld.setAccessible(true); //Make it Accessible to write in field
			Object value = fld.get(obj);
			showln(fld.getName());
			showln("  ||");
			show("  |+- ");
			if (fld.getType().isArray()){
				int length = Array.getLength(value);  
				for (int i = 0; i < length; i++)  
				{  
				    Object element = Array.get(value, i);
				    show(element.toString()+" ");
				}  
			}
			else {	show(value.toString());}
			showln("");
			
		}
	}
	
	public static void show(String aWhat) {
		System.out.print(aWhat);
	}
	public static void showln(String aWhat) {
		System.out.println(aWhat);
	}
}
