package L2.Lang;

import java.util.Scanner;

public class Launcher {

	public static void main(String args[]) throws Exception {
//		String in = ask("STring");
//		System.out.println(Register.main(in));

		Cat Barsik = new Cat();
		Cat Mur4ik = new Cat(5, 1, 2, 3);
		int rgbc[] = { 1, 2, 3 };
		Cat Massik = new Cat(5, rgbc);
		System.out.println("Barsik: " + Barsik.toString());
		System.out.println("Mur4ik: " + Mur4ik.toString());
		System.out.println("Massik: " + Massik.toString());
		System.out.println(Barsik.equals(Massik));
		System.out.println(Massik.equals(Mur4ik));
		System.out.println(Mur4ik.equals(Mur4ik));

//		Exce1 e1 = new Exce1();
//		throw e1;
		Exce2 e2 = new Exce2();
		throw e2;
		

	}

	public static String ask(String aWhat) {
		System.out.print(aWhat);
		Scanner in = new Scanner(System.in);
		return in.nextLine();
	}
	
	public static void show(String aWhat) {
		System.out.println(aWhat);
	}
}
