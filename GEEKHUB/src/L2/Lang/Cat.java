package L2.Lang;

import java.util.Arrays;

public class Cat extends Launcher {

	private int rgb[] = new int[3];
	private int age;

	public Cat() {
		rgb[0] = 0;
		rgb[1] = 0;
		rgb[2] = 0;
		age = 0;
	}

	public Cat(int Age, int rgbColor[]) {
		int l = rgbColor.length;
		if (l < 3) {
			for (int i = 0; i < l; i++)
				rgb[i] = rgbColor[i];
			for (int i = l - 1; i < 3; i++)
				rgb[i] = 0;
		} else {
			rgb[0] = rgbColor[0];
			rgb[1] = rgbColor[1];
			rgb[2] = rgbColor[2];
		}
		age = Age;

	}

	public Cat(int Age, int R, int G, int B) {
		rgb[0] = R;
		rgb[1] = G;
		rgb[2] = B;
		age = Age;
	}

	public String toString() {
		return "Age:" + this.age + "  " + "Color(RGB):" + this.rgb[0] + ","
				+ this.rgb[1] + "," + this.rgb[2];
	}

	public boolean equals(Cat c2) {
		if (this instanceof Cat){
			if (this.hashCode() == c2.hashCode())
				return true;
			if ((this.age == (c2.age)) && (Arrays.equals(this.rgb, c2.rgb)))
				return true;
			else
				return false;
		}
		else return false;
	}

}
