package L2.BallotBox;

import java.util.ArrayList;
import java.util.List;

public class BallotBox implements Comparable<Vote> {

	private List<Vote> box = new ArrayList<Vote>();
	private String constant1 = "Vlizlo";
	private String constant2 = "Odarych";

	public boolean toVote(Vote V) {
		if (V.getName().equals(constant1))
			return true;
		else if (V.getName().equals(constant2)) {
			box.add(V);
			box.add(V);
			return true;
		} else if (checkvalid(V.getName())) {
			box.add(V);
			return true;
		} else
			return false;
	}

	public void showAll() {
		System.out.println("Total votes: " + box.size());
		for (int i = 0; i < box.size(); i++) {
			System.out.println(box.get(i).getName());
		}
	}

	public int compareTo(Vote V) {
		if (box.contains(V))
			return 1;
		else
			return 0;
	}

	public boolean checkvalid(String aName) {
		boolean checkvalid = true;
		if (aName.length() == 0)
			return false;
		// Check Acc validness
		int b = 0;
		boolean b1, b2, b3, b4;
		for (int i = 0; i < aName.length(); i++) {

			b = (int) aName.charAt(i);
			b1 = (b >= 97) & (b <= 122);
			b2 = (b >= 65) & (b <= 90);
			if (i == 0) {
				if ((b1 | b2) == false)
					return false;
				// {throw new
				// Exception("First char of Candidate is not from {a..z},{A..z}");}
			}
			b3 = (b >= 48) & (b <= 57);
			b4 = (b == 95);
			if ((b1 | b2 | b3 | b4) == false) {
				// throw new
				// Exception("Candidate name contains unacceptable chars");
				return false;
			}
		}
		return checkvalid;
	}

}
