package L2.BallotBox;

import java.util.Scanner;

public class Launcher {

	public static void main(String args[]) throws Exception {
		BallotBox CVK = new BallotBox();
		Vote l1 = new Vote("Julia");
		Vote l2 = new Vote("Klichko");
		Vote l3 = new Vote("Vasya66");
		Vote l4 = new Vote("1Vasya66");
		Vote l5 = new Vote("Vlizlo");
		Vote l6 = new Vote("Odarych");
		CVK.toVote(l1);
		CVK.toVote(l2);
		CVK.toVote(l3);
		CVK.toVote(l4);
		CVK.toVote(l5);
		CVK.toVote(l6);
		CVK.showAll();
		System.out.println("Voted for Vlizlo: " + CVK.compareTo(l5));
	}
}
