package L2.Stack;

import java.util.Scanner;

public class Launcher {

	public static void main(String args[]) throws Exception {
		Stack S = new Stack();
		S.add(false);
		S.add(true);
		S.add(false);
		S.add(true);
		S.add(true);
		System.out.println(S.size());
		System.out.println("Peek=" + S.peek().toString());
		System.out.println(S.poll().toString());
		System.out.println(S.size());
		System.out.println(S.poll().toString());
		System.out.println(S.size());
		System.out.println(S.poll().toString());
		System.out.println(S.size());
		System.out.println(S.poll().toString());
		System.out.println(S.size());
		System.out.println(S.poll().toString());
		System.out.println(S.size());
		System.out.println(S.poll().toString());
		System.out.println(S.size());
	}

	public static String ask(String aWhat) {
		Scanner in = new Scanner(System.in);
		return in.nextLine();
	}
}
