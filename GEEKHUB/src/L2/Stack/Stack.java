package L2.Stack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

public class Stack implements Queue {

	private List<Object> stack = new ArrayList<Object>();
	private int count = 0;
	private Object emptystack = new Object();

	// REQUIRED
	public boolean add(Object Ob) {
		try {
			stack.add(Ob);
			count++;
			return true;
		} catch (Exception e) {
			System.out.println("Fail");
			return false;
		}

	}

	public Object poll() {
		if (count >= 1) {
			Object e = new Object();
			e = stack.get(count - 1);
			stack.remove(count - 1);
			count--;
			return e;
		} else
			return emptystack;
	}

	public Object peek() {
		try {
			return stack.get(count - 1);
		} catch (Exception e) {
			return null;
		}
	}

	// REST

	public boolean retainAll(Collection e) {
		try {
			stack.retainAll(e);
			return true;
		} catch (Exception er) {
			return false;
		}

	}

	public boolean removeAll(Collection C) {
		try {
			stack.removeAll(C);
			return true;
		} catch (Exception er) {
			return false;
		}
	}

	public boolean contains(Object O) {
		try {
			stack.contains(O);
			return true;
		} catch (Exception er) {
			return false;
		}
	}

	// Dummy

	public Iterator iterator() {
		Iterator I;
		I = stack.iterator();
		return I;
	}

	public boolean addAll(Object Obj) {
		return false;
	}

	public boolean addAll(Collection Obj) {
		return false;
	}

	public boolean containsAll(Object Obj) {
		return false;
	}

	public boolean containsAll(Collection Obj) {
		return false;
	}

	public static void returnAll() {

	}

	public static void retainAll() {

	}

	public int size() {
		return count;
	}

	public boolean isEmpty() {
		return false;
	}

	public Object element() {
		return stack.getClass();
	}

	public boolean remove(Object Obj) {
		return false;
	}

	public Object remove() {
		return false;
	}

	public void clear() {

	}

	public static void removeAll() {

	}

	public Object[] toArray(Object[] Obj) {
		Object[] lol = new Object[2];
		lol[1] = new Object();
		lol[2] = new Object();
		return lol;
	}

	public Object[] toArray() {
		return stack.toArray();

	}

	public boolean offer(Collection Obj) {
		return false;
	}

	public boolean offer(Object Obj) {
		return false;
	}
}
