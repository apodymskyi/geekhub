package L2.Garage;

public class Car implements Comparable<Car> {

	private String name;
	private int base;
	private String color;

	private static int sortParametr = 0;

	public Car(String aName, String aColor, int aBase) {
		name = aName;
		base = aBase;
		color = aColor;
	}

	public String getName() {
		return this.name;
	}

	public int getBase() {
		return this.base;
	}

	public String getColor() {
		return this.color;
	}

	public int compareTo(Car C) {
		switch (sortParametr) {
		// by Name
		case 0: {
			if (this.getName().codePointAt(0) < C.getName().codePointAt(0))
				return -1;
			else if (this.getName().codePointAt(0) == C.getName()
					.codePointAt(0))
				return 0;
			else
				return 1;
		}
		case 1: {
			if (this.getColor().codePointAt(0) < C.getColor().codePointAt(0))
				return -1;
			else if (this.getColor().codePointAt(0) == C.getColor()
					.codePointAt(0))
				return 0;
			else
				return 1;
		}
		case 2: {
			if (this.getBase() < C.getBase())
				return -1;
			else if (this.getBase() == C.getBase())
				return 0;
			else
				return 1;
		}
		}
		return 0;
	}

	public static void compareParam(int i) {
		sortParametr = i;
	}
}
