package L2.Garage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import L2.Garage.Car;

public class Garage {

	private List<Car> garage = new ArrayList<Car>();

	// Sorting by 0-Name, 1-Color, 2 - Base

	public void addCar(Car C) {
		garage.add(C);
	}

	public void sortbyName() {
		Car.compareParam(0);
		Collections.sort(garage);
	}

	public void sortbyColor() {
		Car.compareParam(1);
		Collections.sort(garage);
	}

	public void sortbyBase() {
		Car.compareParam(2);
		Collections.sort(garage);
	}

	public void showList() {
		for (int i = 0; i < garage.size(); i++)
			System.out.println(i + ": " + garage.get(i).getName() + " "
					+ garage.get(i).getColor() + " " + garage.get(i).getBase());
	}

}