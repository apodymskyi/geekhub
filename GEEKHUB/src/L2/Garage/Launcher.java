package L2.Garage;

import java.util.Scanner;

public class Launcher {

	public static void main(String args[]) throws Exception {
		Car C1 = new Car("Kalina", "red", 10);
		Car C2 = new Car("T5", "blue", 20);
		Car C3 = new Car("ArlenCar", "red", 10);
		Car C4 = new Car("Van", "blue", 15);
		Garage G = new Garage();
		G.addCar(C1);
		G.addCar(C2);
		G.addCar(C3);
		G.addCar(C4);
		G.sortbyBase();
		G.showList();
		G.sortbyName();
		G.showList();
		G.sortbyColor();
		G.showList();

	}

	public static String ask(String aWhat) {
		Scanner in = new Scanner(System.in);
		return in.nextLine();
	}
}
