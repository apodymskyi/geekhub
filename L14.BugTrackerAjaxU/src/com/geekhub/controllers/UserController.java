package com.geekhub.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



import com.geekhub.beans.User;
import com.geekhub.services.UserDao;



@Controller
public class UserController {

	@Autowired UserDao userDAO;
	
	
	@RequestMapping(value="listUsers.html")
	public String list(ModelMap map){
		map.put("users", userDAO.getUserList());
		return "users";
	}
	
	@RequestMapping(value="loadUser.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map){
		User user = id ==null ? new User(): userDAO.getUserById(id);
		map.put("user", user);
		return "user";
	}
	
	@RequestMapping(value="deleteUser.html")
	public String delete(@RequestParam(value="id", required=true) Integer id){
		userDAO.deleteUser(id);
		return "redirect:listUsers.html";
	}
	
	@RequestMapping(value="saveUser.html")
	public String save(User user){
		userDAO.saveUser(user);
		return "redirect:listUsers.html";
	}
	
	@RequestMapping(value="validateUser.html")
	public @ResponseBody String validateUser(@RequestParam(value="login", required=true) String login){
		List<String> users = userDAO.getUsersLogins();
		String answer="ok";
		if (users.contains(login))
			answer="bad";
		return answer;
	}
	
}

