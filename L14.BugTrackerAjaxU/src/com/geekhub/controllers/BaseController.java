package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.geekhub.services.TicketDao;
import com.geekhub.services.UserDao;



@Controller
public class BaseController{

	@Autowired UserDao userDao;
	@Autowired TicketDao ticketDao;
	
	

	@RequestMapping(value="index.html")
	public String index(ModelMap map){
		map.put("users", userDao.getUserList().size());
		map.put("tickets",ticketDao.getTicketList().size());
		return "index";
	}
	
	
	
	
}
