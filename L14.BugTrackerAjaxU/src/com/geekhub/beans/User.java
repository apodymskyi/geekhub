package com.geekhub.beans;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name="USER")
public class User {


	@GeneratedValue
	@Id
	@Column(name="ID")
    private Integer id;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="LOGIN")
    private String login;
	
	@Column(name="PASSWORD")
    private String password;

	@Column(name="NAME")
    private String name;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
