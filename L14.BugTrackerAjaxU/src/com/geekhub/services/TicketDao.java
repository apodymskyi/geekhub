package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Ticket;

public interface TicketDao {

	
	public List<Ticket> getTicketList();
	public Ticket getTicketById(Integer id);
	public void deleteTicket (Integer id);
	public void saveTicket (Ticket ticket);
}
