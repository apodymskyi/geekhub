package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.geekhub.beans.User;

public interface UserDao {

	public List<User> getUserList();	
	public User getUserById(Integer id);	
	public void deleteUser(Integer id);		
	public void saveUser(User user);
	
	public List <String> getUsersLogins();

}