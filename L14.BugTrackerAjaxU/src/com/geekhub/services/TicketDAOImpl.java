package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Ticket;

@Repository
@Transactional
public class TicketDAOImpl implements TicketDao {
	
	@Autowired SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	

	public List<Ticket> getTicketList() {
		return sessionFactory.getCurrentSession().createQuery("from Ticket").list();
	}

	public Ticket getTicketById(Integer id) {
		return (Ticket) sessionFactory.getCurrentSession().get(Ticket.class, id);
	}

	public void deleteTicket(Integer id) {
		Ticket ticket = getTicketById(id);
		if (null != ticket)
			sessionFactory.getCurrentSession().delete(ticket);
		
	}

	public void saveTicket(Ticket ticket) {
		sessionFactory.getCurrentSession().saveOrUpdate(ticket);
		
	}

}
