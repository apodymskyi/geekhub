
<script type="text/javascript" src="js/valid.reg.info.js"> </script>
<script type="text/javascript" src="js/valid.unique.login.js"> </script>
<style>
.error {
    float: none;
    color: red;
    padding-left: .5em;
    vertical-align: top;
    display: block;
}
.field {
	width: 300px;
	background-color: white;
	align: center;
}
.green {
    float: none;
    color: green;
    padding-left: .5em;
    vertical-align: top;
    display: block;
}
</style>
<form id="Form" action="/HotelBookingService/saveUser.html" class="ajax">
	<H2 style="width: 100%;" align="center">REGISTRATION</H2>
	<table style="width: 100%">
		<tr>
			<td>E-MAIL</td>
			<td><input type="text" value="${user.email}" name="email" id="email" class="field" onchange="checkLogin()"><span id="check"></span></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><input type="text" value="${user.password}" name="password" class="field"></td>
		</tr>
		<tr>
			<td>First Name</td>
			<td><input type="text" value="${user.firstName}" name="firstName" class="field"></td>
		</tr>
		<tr>
			<td>Last Name</td>
			<td><input type="text" value="${user.lastName}" name="lastName" class="field"></td>
		</tr>
		<tr>
			<td>Phone</td>
			<td><input type="text" value="${user.phone}" name="phone" class="field"></td>
		</tr>
		<tr>
			<td>My role:</td>
		<td>
			<input type="radio" name="role" value="ROLE_USER" checked> Customer
			<input type="radio" name="role" value="ROLE_HOTEL"> Hotel Owner
		</td>
		</tr>
	</table>
	
	
	
	 
	<input type="hidden" name="id" value="${user.id}">
	<br>
	<div align=center>
	<input type="submit" value="Register" id="submit" style="width: 50%; font-weight:bold; font-size: 20px">
	</div>
</form>