<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<!-- CommentsSupport -->
	<script type="text/javascript" src="js/comments.js"> </script> 
	<script type="text/javascript">
		$(document).ready(function() {
			showHotelComments();
		});
	</script>




<title>HotelInfo</title>

</head>
<body>
	<table border="1px" width="100%">
	<tr>
		<td style="color:blue;text-align:center;font-size:30px" colspan="3"><strong> ${hotel.name} </strong></td>
	</tr>
	<tr>
		<td><strong>Country:</strong> ${hotel.country}</td>
		<td><strong>City:</strong> ${hotel.city}</td> 
		<td><strong>Owner:</strong> ${hotel.owner.firstName} ${hotel.owner.lastName}</td>
	</tr>
	<tr>
		<td colspan="3"><strong>Description:</strong> ${hotel.description} </td>
	</tr>
	</table>

<div id="hotelPhotos" style="height: 130px; overflow-x: auto;">
				<c:forEach items="${photos}" var="id">
					<a href="./getPhoto.html?id=${id}" rel="lightbox[Hotel photos]" title="hotel photo"> <img src="./getPhoto.html?id=${id}" style="max-height: 100%; max-width: 100%"></a>
					</c:forEach>
			</div>	


<form action="./addHotelComment.html" id="commentForm" class="ajaxdivupdate" divToUpdate="comments">

	
	<input type="hidden" name="hotel.id" value="${hotel.id}">
	<input type="hidden" name="id" value="${comment.id}">
	Show <input type="number" step="10" min="10" max="100" value="10" name="showResults"  onchange="showHotelComments()" class="ajaxdivupdate" divToUpdate="comments"> comments
	<sec:authorize access="isAuthenticated()">
		<textarea rows="2" value="${comment.text}" name="text" class="field"  maxlength="100" style='width:98%; resize:none;'></textarea>
		<input type="submit" value="Comment" id="submit">
	</sec:authorize>
</form>


<div id="comments"></div>
</body>
</html>