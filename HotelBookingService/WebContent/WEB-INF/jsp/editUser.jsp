<form id="Form" action="/HotelBookingService/saveUser.action" class="ajax">
	<H2 style="width: 415px;" align="center">Edit User</H2>
	<table>
		<tr>
			<td>E-MAIL</td>
			<td><input type="text" value="${user.email}" name="email" style="width: 300px; "></td>
		</tr>
		<tr>
			<td>First Name</td>
			<td><input type="text" value="${user.firstName}" name="firstName" style="width: 300px; "></td>
		</tr>
		<tr>
			<td>Last Name</td>
			<td><input type="text" value="${user.lastName}" name="lastName" style="width: 300px; "></td>
		</tr>
		<tr>
			<td>Phone</td>
			<td><input type="text" value="${user.phone}" name="phone" style="width: 300px; "></td>
		</tr>
		<tr>
			<td>Role</td>
			<td><select name="role">
			<option value="${user.role}">${user.role}</option>
			<option value="ROLE_USER">ROLE_USER</option>
			<option value="ROLE_HOTEL">ROLE_HOTEL</option>
			<option value="ROLE_ADMIN">ROLE_ADMIN</option>
			</select></tr>
		<tr>
	</table>
	
	<input type="hidden" name="id" value="${user.id}">
	<input type="hidden" name="password" value="${user.password}">

	<input type="submit" value="Save" id="submit" style="width: 188px">
</form>