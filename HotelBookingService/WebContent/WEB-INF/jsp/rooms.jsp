<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<center><h2>${hotel.name}'s room list</h2></center>


<table border="1" style="width:100%">
    <tr>
        <th><img src="img/roomNumber.png"></th>
        <th><img src="img/occupancy.png"></th>
        <th><img src="img/roomType.png"></th>
        <th><img src="img/price.png"></th>
        <th></th>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
        		<th colspan="2">Manage</th>
        		</sec:authorize>
    </tr>

<c:forEach items="${rooms}" var="room">
    <tr>
        <td>${room.roomNumber}</td>
        <td>${room.occupancy}</td>
        <td>${room.type}</td>
        <td>${room.price}</td>
        <td><a href="/HotelBookingService/roomInfo.html?id=${room.id}" class="ajax">Info</a></td>
               
        <sec:authorize access="hasRole('ROLE_ADMIN')">
        	<td><a href="/HotelBookingService/loadRoom.html?id=${room.id}" class="ajax">Edit</a></td>
        	<td><a href="/HotelBookingService/deleteRoom.html?id=${room.id}" class="ajax">Delete</a></td>
		</sec:authorize>
    </tr>
</c:forEach>

</table>