<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="js/datePicker.js"> </script>
<script type="text/javascript" src="js/valid.booking.anonymous.js"> </script>
<script type="text/javascript" src="js/valid.unique.login.js"> </script>
<script type="text/javascript" src="js/valid.room.free.js"> </script>
<script type="text/javascript" src="js/valid.booking.cost.js"> </script>
<script type="text/javascript" src="js/submit.booking.anonymous.js"></script>

<link rel="stylesheet" type="text/css" href="css/error.css">

<br>
<h1 align=center>Add Booking:</h1>
<br>


<form action="" id="Form" class="ajax">
	<div id="errorContainer">
	<p style="color:red;">Please correct the following errors and try again:</p>
    <ul />
	</div>
	
	
	<div>
	Hotel:
	<input type="hidden" name="hotel.id" value="${hotel.id}"> ${hotel.name}
	</div>
	<div>
	Room:
	<input type="hidden" id="roomId" name="room.id" value="${room.id}"> ${room.roomNumber} (id: ${room.id})
	</div>
	<div>
	DateIN: <input type="text" id="dateIn" value="<fmt:formatDate value="${booking.dateIn}" pattern="yyyy-MM-dd"/>" name="dateIn" onchange="checkRoomToBeFree()" class="datepicker">
	</div>
	<div>
	DateOut: <input type="text" id="dateOut" value="<fmt:formatDate value="${booking.dateOut}" pattern="yyyy-MM-dd"/>" name="dateOut" onchange="checkRoomToBeFree()"class="datepicker"><br>
	</div>
	<div id="checkRoomFree">
	</div>
	<input type="hidden" name="status" value="NOT_CONFIRMED" >    
	<input type="hidden" name="id" value="${booking.id}">
	
	<div id="addUser">
	<H2 style="width: 100%;" align="center">Contact Information:</H2>
	<table style="width: 100%">
		<tr>
			<td>E-MAIL</td>
			<td><input type="text" value="${user.email}" name="email" id="email" class="field" onchange="checkLogin()"><span id="check"></span></td>
		</tr>
		<tr>
			<td>First Name</td>
			<td><input type="text" value="${user.firstName}" name="firstName" class="field"></td>
		</tr>
		<tr>
			<td>Last Name</td>
			<td><input type="text" value="${user.lastName}" name="lastName" class="field"></td>
		</tr>
		<tr>
			<td>Phone</td>
			<td><input type="text" value="${user.phone}" name="phone" class="field"></td>
		</tr>
	</table>
		<input type="hidden" name="id" value="${user.id}">
		<input type="hidden" name="role" value="ROLE_USER" checked>
		<input type="hidden" name="password" value="default">
</div>
	
	
	
	
	<input type="submit" value="Reserve" id="submit">
	<input type="submit" value="Pay Now" id="pay">
</form>
Current room price: <input type="text" value="${room.price}" readonly="readonly" id="roomPrice">
<div id="state"></div>
<div id="cost"></div>
<div id="payment"></div>