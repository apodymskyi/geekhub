<script type="text/javascript" src="js/valid.payment.form.js"></script>

<link rel="stylesheet" type="text/css" href="css/error.css">

<br>
<h1>Credit Card Transaction Form</h1>
	<div>
		<form action="./create_transaction.html" method="POST" id="braintree-payment-form" class="ajax">
				<div id="errorContainer">
					<p style="color:red;">Please correct the following errors and try again:</p>
    				<ul />
				</div>
				
			<p>
				<label>Card Number</label> <input type="text" size="20"
					autocomplete="off" name="number" />
			</p>
			<p>
				<label>CVV</label> <input type="text" size="4" autocomplete="off"
					name="cvv" />
			</p>
			<p>
				<label>Expiration (MM/YYYY)</label> <input type='text' name='date'
					id='credit_card_exp'></input>
			</p>
			<input type="hidden" name="id" value="${bookingId}">
			<input type="submit" id="submit" value="Confirm"/>
			
		</form>
	</div>
	<script type="text/javascript" src="js/braintree.js"></script>
	<script type="text/javascript">
		var braintree = Braintree.create("MIIBCgKCAQEAqO9BmJJiZ7HDVKYPneOrXO6iyMOIb5heQhCWuVkw/3Oc0PGKteF2+tj1QfYXOs80y4VqJcn2M4KloOJ1dkUH4fLtqj9Nr4EVKKziv8Ig1Mw2+bH/BWw0UBV3A0P3NVspovqkTB9A/ol9EIcUelVGCoRHyYPDL8hDvJ65/u1z73zj36+1p+9fxSMo1u+8rTuEtcFge7/bCWwf+jWERn+7/dwwZa0aRSm9avyHUSs5FJ1aIorayzi88WKcuHcsc+7AkcE7sojYc8Wz243bgOl10vJCI3aHN0HnlFoknbQASueYQupy1IguDspfLf0bqV9FeiE3+gKMym21r9hJ3PNYaQIDAQAB");
		braintree.onSubmitEncryptForm('braintree-payment-form');
	</script>