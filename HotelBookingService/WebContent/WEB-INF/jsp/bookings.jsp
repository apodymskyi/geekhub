<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="js/datePicker.js"> </script> 

${status}
<form action="/HotelBookingService/listBookings.html" id="filterBookings" class="ajax">
Search Criteria: <br>
	DateIn: <input type="text" value="" name="date" class="datepicker" readonly="readonly">
	 by hotel:
		<select name="hotel.id">
			 <option value="">None</option>
   				 <c:forEach items="${hotels}" var="hotel">
     				  <option value="${hotel.id}">${hotel.name} (${hotel.id})</option>
   				 </c:forEach>
    	</select><br>
	<input type="submit" value="Filtrate">
</form>


<br>
<br>
Bookings:
<div class="bookings">
<c:forEach items="${bookings}" var="booking">
   <div id = "booking">
	<div id = "b.id">Book:${booking.id}</div>
	<div id = "o.id">Owner:${booking.owner.id} <br></div>
	<div id = "r.id">Room:${booking.room.id} <br></div>
	<div id = "h.id">Hotel:${booking.hotel.id} ${booking.hotel.name} <br></div>
	<div id = "date">Date In: <fmt:formatDate value="${booking.dateIn}" pattern="dd-MM-yyyy" /> <br></div>
	<div id = "date">Date Out:<fmt:formatDate value="${booking.dateOut}" pattern="dd-MM-yyyy" /> <br></div>
	<div id = "status">${booking.status} <br></div>
           <p style="text-align: right;"> <a href="/HotelBookingService/loadBooking.html?id=${booking.id}" class="ajax">Edit</a> 
			<a href="/HotelBookingService/deleteBooking.html?id=${booking.id}" class="ajax">Delete</a> <br> </p>
    </div>   
	<hr>
</c:forEach>
</div>
   