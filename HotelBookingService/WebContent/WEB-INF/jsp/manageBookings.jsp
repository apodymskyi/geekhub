<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type="text/javascript">
function hide(whatClass){
	$("."+whatClass).hide();
}
function show(whatClass){
	$("."+whatClass).show();
}

</script>

<script type="text/javascript">
function updateSearch(){
	var formerUrl=$('#hotel').attr('url');
	var addToUrl='&show='+$("input[name='limit']").val();
	if (formerUrl==null){
			$(".manageBookings").html('<img src="img/loading.gif">');
			$.ajax({
				url: 'manageBookings.html?id=${hotelID}'+addToUrl,
				type: "GET",
				dataType: "html",
				success: function(response) {
					$(".manageBookings").html(response);
					}			
			});			
	}
	else {$('#hotel').attr('url',formerUrl+addToUrl);};
};
</script>

<div class="manageBookings">
<h1 align=center>Manage bookings</h1>

<a onclick="hide('CONFIRMED')">Hide Confirmed</a>
<br>
<a onclick="hide('NOT_CONFIRMED')">Hide NOT_Confirmed</a>
<br>
Show <input type="number" step="10" min="10" max="50" value="${showResults}" name="limit" class="field" onchange="updateSearch()"> bookings
<br>


<h4 align=center>${status}</h4>
<sec:authorize access="hasAnyRole('ROLE_HOTEL','ROLE_ADMIN')">
	<div class="hotels">
		<c:forEach items="${hotels}" var="hotel">
			<div id="hotel" class="buttonElement"
				url="manageBookings.html?id=${hotel.id}"
				onmouseover="this.style.color='blue'"
				onmouseout="this.style.color='black'">
				<img src="img/hotel.png"> ${hotel.name}<br> <img
					src="img/country.png"> ${hotel.country} <img
					src="img/city.png"> ${hotel.city}<br>
			</div>
			
		</c:forEach>
	</div>
	<br>
	<br>

	<div class="bookings">
		<table border="1px" style="width: 100%;">
			<tr>
				<th>DateIn</th>
				<th>DateOut</th>
				<th>Room</th>
				<th>E-mail<br> Phone
				</th>
				<th>Status</th>
				<th><img src="img/visa-mc.jpg"></th>
				<th>Action</th>
			</tr>
			<c:forEach items="${bookings}" var="booking">
			
				<tr>
					<td><div class="${booking.status}"><fmt:formatDate value="${booking.dateIn}" pattern="dd/MM/yy" /></div></td>
					<td><div class="${booking.status}"><fmt:formatDate value="${booking.dateOut}" pattern="dd/MM/yy" /></div></td>
					<td><div class="${booking.status}">${booking.room.roomNumber}</div></td>
					<td><div class="${booking.status}">${booking.owner.email}<br>${booking.owner.phone}</div></td>
					<td><div class="${booking.status}">${booking.status}</div></td>
					<td><div class="${booking.status}">
						<c:set var="payment" value="${booking.payment}" /> 
						<c:if test="${not empty payment}">
						${payment.name}<br>			
						${payment.cost}$ (${payment.id})
						</c:if>
						</div>
					</td>
					<td><div class="${booking.status}">
						<a href="./deleteBooking.html?id=${booking.id}" class="ajax">Delete</a><br> 
						<a href="./loadBooking.html?id=${booking.id}" class="ajax">Edit</a>
						</div>
					</td>
				</tr>
				
			</c:forEach>
		</table>
	</div>
</sec:authorize>
</div>