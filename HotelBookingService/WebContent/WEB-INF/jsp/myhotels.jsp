<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript" src="js/valid.fileformat.js"></script>

    <table border="1">
    <tr>
        <th>Hotel Name</th>
        <th>Country</th>
        <th>City</th>
        <th colspan="3">Manage</th>
    </tr>

<c:forEach items="${hotels}" var="hotel">
    <tr>
        <td>${hotel.name}</td>
        <td>${hotel.country}</td>
        <td>${hotel.city}</td>
        <td><a href="./loadHotel.html?id=${hotel.id}" class="ajax">Edit</a></td>
        <td colspan="2"><a href="./addRoom.html?id=${hotel.id}" class="ajax">Add Room</a></td>
    </tr>
    <tr>
    <td colspan="3">
	    <form id="upload" enctype="multipart/form-data" method="POST" target="status" action="./uploadImage.html?scope=hotel&id=${hotel.id}">
	    Add photo <input onchange="Checkfiles()" type="file" name="file" id="file" accept="image/*, image/jpeg">
	    <input type="submit" value="Upload">

	    </form>
	</td>
    <td><a href="./hotelInfo.html?id=${hotel.id}" class="ajax">Hotel Info</a></td>
	<td><a href="./myrooms.html?id=${hotel.id}"class="ajax">Rooms</a></td>
	<td><a href="./managePhoto.html?scope=hotel&id=${hotel.id}" class="ajax">Manage Photos</a></td>
    </tr>
</c:forEach>
</table>
<a href="./loadHotel.html" class="ajax">Add Hotel</a>
<br>
<iframe name="status" height="50" allowTransparency frameborder="0"></iframe>
