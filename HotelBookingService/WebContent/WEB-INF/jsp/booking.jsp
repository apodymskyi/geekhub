<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="js/datePicker.js"> </script>
<script type="text/javascript" src="js/valid.booking.date.js"> </script>
<script type="text/javascript" src="js/valid.room.free.js"> </script>

<script type="text/javascript" src="js/submit.booking.logged.js"></script>
<link rel="stylesheet" type="text/css" href="css/lightbox.css">

<link rel="stylesheet" type="text/css" href="css/error.css">


<script type="text/javascript" src="js/valid.booking.cost.js"> </script>

    <br>
	<h2 align=center>Room info:</h2>
	<br>
<div>
	<table border="1px" style="width: 100%;">
		<tr>
			<td style="color: blue; text-align: center; font-size: 30px"
				colspan="3"><strong> Hotel:${room.hotel.name} <br>
					Room: ${room.roomNumber} </strong></td>
		</tr>
		<tr>
			<td><strong>Occupancy:</strong> ${room.occupancy}</td>
			<td><strong>Type:</strong> ${room.type}</td>
			<td><strong>Price:</strong> ${room.price}</td>
		</tr>
		<tr>
			<td colspan="3"><strong>Description:</strong>${room.description}</td>
		</tr>
	</table>
	<div style="border-style:double;">
		<div id="roomPhotos" style="height: 100px; overflow-x: auto;">
		<c:forEach items="${photos}" var="id">
			<a href="./getPhoto.html?id=${id}" target="_blank" rel="lightbox[room]" title="room"> 
			<img src="./getPhoto.html?id=${id}" style="max-height: 100%; max-width: 100%"></a>
		</c:forEach>
		</div>
	</div>
</div>

<br>
<br>
<h4>Availability</h4>

<form action="" id="Form" class="ajax">
	<div id="errorContainer">
	<p style="color:red;">Please correct the following errors and try again:</p>
    <ul />
	</div>
    <input type="hidden" name="owner.id" value="${loggedUser.id}"> 
	<input type="hidden" name="hotel.id" value="${room.hotel.id}">
	<input type="hidden" id="roomId" name="room.id" value="${room.id}">
	<div>
	DateIN: <input type="text" id="dateIn" value="<fmt:formatDate value="${booking.dateIn}" pattern="yyyy-MM-dd"/>" name="dateIn" class="datepicker" onchange="checkRoomToBeFree()">
	</div>
	<span>
	DateOut: <input type="text" id="dateOut" value="<fmt:formatDate value="${booking.dateOut}" pattern="yyyy-MM-dd"/>" name="dateOut" class="datepicker" onchange="checkRoomToBeFree()"><br>
	</span>
	<div id="checkRoomFree"></div>
	<input type="hidden" name="status" value="NOT_CONFIRMED" >    
	<input type="hidden" name="id" value="${booking.id}">
	
	<input type="submit" value="Reserve" id="submit">
	<input type="submit" value="Pay Now" id="pay">
</form>

Current room price: <input type="text" value="${room.price}" readonly="readonly" id="roomPrice" size="2">
<div id="state"></div>
<div id="state"></div>
<div id="cost"></div>

