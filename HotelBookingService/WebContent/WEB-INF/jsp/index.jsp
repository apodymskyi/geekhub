<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<title>Hotel Booking Service</title>


	<script src="js/jquery.1.7.min.js"></script>
	<script src="js/jquery.form.js"></script> 
 	<script src="js/jquery.validate.js"> </script>
 	<script src="js/jquery-ui.js"></script>
 	
 	<script type="text/javascript" src="js/datePicker.js"> </script>
 	<script src="js/localizationDatepicker.js"></script>
 	 	
 	<script type="text/javascript" src="js/main.js"> </script> 
 	
 	<!--  Style -->
 	<link rel="stylesheet" type="text/css" href="css/main.css">
 	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
 	
 	<!-- PhotoSupport -->
	<script type="text/javascript" src="js/lightbox.js"> </script> 
	<link rel="stylesheet" type="text/css" href="css/lightbox.css">

	<script type="text/javascript" src="js/jquery.tinysort.min.js"> </script> 

	
</head>

<body>
    <div id="wrapper">

    <div id="header">Hotel Booking Service</div>
    
    	
	  <div id="column-left">
        <div class="button" onclick="location.href='/HotelBookingService'" onmouseover="this.style.color='red'" onmouseout="this.style.color='black'">
            General
        </div>
        <div class="button" url="find-a-hotel.html" onmouseover="this.style.color='red'" onmouseout="this.style.color='black'">
            Hotels
        </div>
        <div class="button" id="myHotels" url="find-a-room.html" onmouseover="this.style.color='red'" onmouseout="this.style.color='black'">
            Find-a-Room
        </div> 
        
        
        <hr>
        
        <sec:authorize access="isAuthenticated()">
       	  <div class="button" url="myBookings.html" onmouseover="this.style.color='red'" onmouseout="this.style.color='black'">
            	My Bookings
        	  </div>
        </sec:authorize>
        
               
        
        
        <sec:authorize access="hasRole('ROLE_ADMIN')">
        <div class="button" id="userList" url="./listUsers.action" onmouseover="this.style.color='red'" onmouseout="this.style.color='black'">
            Users List
        </div>
       
        </sec:authorize>
        
        <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_HOTEL')">  	
        <br>
        <br>
        
        
        		<div class="button" id="myHotels" url="listMyHotels.html" onmouseover="this.style.color='red'" onmouseout="this.style.color='black'">
         		   My Hotels
       			</div>
       			<div class="button" id="myHotels" url="manageBookings.html" onmouseover="this.style.color='red'" onmouseout="this.style.color='black'">
         		   Manage Bookings
       			</div>
       			
        </sec:authorize>
 
       
    </div>

    <div id="column-center">
   	<h1 align=center>WELCOME</h1>
   			
    </div>
    

 
	<div id="column-right">
	 <sec:authorize access="isAnonymous()">
	 	<div class="button" url="./register.html" onmouseover="this.style.color='red'" onmouseout="this.style.color='black'">
            Registration
        </div>
      
		
        <div id="login_form">
            <h1 align=center>Login</h1>
            <form name='f' action="<c:url value='j_spring_security_check' />" method='POST'>
                <div>
                    E-MAIL:&nbsp;<input type="text" name="j_username" />
                </div>
                <div>
                    Password:&nbsp;<input type='password' name='j_password' />
                </div>
                <div>
                    <input name="submit" type="submit" value="Submit" /> 
                    <input name="reset" type="reset" value="Reset"/>
                </div>
            </form>
			<h3 style="font-size:20; color:#FF1C19;">${error}</h3>
        </div>
       </sec:authorize>
       <sec:authorize access="isAuthenticated()">
		<div style="font-size:20px; margin: 4px 4px; Width: 230px; border: 1px black solid; text-align: center;">
        	Hello, ${loggedUser.firstName} ${loggedUser.lastName}!<br>
			<a href="<c:url value="j_spring_security_logout" />" > <strong>Logout</strong></a>
		</div>       
       </sec:authorize>
        
    </div>
 
    <div id="footer">�GeekHub 2013</div>
    </div>



</body>
</html>
