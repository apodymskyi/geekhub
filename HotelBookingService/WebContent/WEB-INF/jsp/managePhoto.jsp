<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

	<script type="text/javascript" src="js/manage.photo.js"> </script> 

<sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_HOTEL')">
	<table border="1" style="width:100%;">
		<tr>
			<th>Photo</th>
			<th>Size</th>
			<th colspan="2">Action</th>
		</tr>
	
		<c:forEach items="${photos}" var="photo">
		<tr>
			<td>
			<div id="photo${photo.id}">
				<a href="./getPhoto.html?id=${photo.id}" rel="lightbox[photos]" title="photo">${photo.filename}</a>
			</div>
			</td>
				<td><div id="photoSize${photo.id}">${photo.size} byte </div></td>
				<td>
					<div id="photoDelete${photo.id}">
						<a href="./deletePhoto.html?id=${photo.id}" class="ajax"  onclick="deletethisphoto(${photo.id})">Delete</a>
					</div>
				</td>
		
			</tr>
		</c:forEach>
	</table>
</sec:authorize>