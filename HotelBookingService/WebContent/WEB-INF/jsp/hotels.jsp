<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<sec:authorize access="hasRole('ROLE_ADMIN')">
<div>
<table border="1" style="width:100%;">
    
    <tr>
        <th>Hotel Name</th>
        <th>Country</th>
        <th>City</th>
        <th>Details</th>
        <th colspan="3">Administrator View</th>
    </tr>

	<c:forEach items="${hotels}" var="hotel">
	    <tr>
	        <td><img src="img/hotel.png">${hotel.name}</td>
	        <td><img src="img/country.png">${hotel.country}</td>
	        <td><img src="img/city.png">${hotel.city}</td>
			<td><a href="/HotelBookingService/hotelInfo.html?id=${hotel.id}" class="ajax">Hotel Info</a></td>
	        <td>${hotel.owner.firstName} ${hotel.owner.lastName}</td>
	        <td><a href="/HotelBookingService/loadHotel.html?id=${hotel.id}" class="ajax">Edit</a></td>
	        <td><a href="/HotelBookingService/deleteHotel.html?id=${hotel.id}" class="ajax">Delete</a></td>
	    </tr>
	</c:forEach>
</table>
</div>
</sec:authorize>

<div id="allHotelsView">


<sec:authorize access="isAuthenticated()">
	<div class="hotels"> 
 			<c:forEach items="${hotels}" var="hotel">
 			<div id="hotel" class="buttonElement" url="roomsByHotel.html?id=${hotel.id}"  onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'">
 			<img src="img/hotel.png">  ${hotel.name}<br>
 			<img src="img/country.png">  ${hotel.country}<br>
 			<img src="img/city.png"> ${hotel.city}<br>
 			</div>
 			<a href="/HotelBookingService/hotelInfo.html?id=${hotel.id}" class="ajax">Hotel Info</a>
 			</c:forEach>
 	</div>
 </sec:authorize>
 
 <sec:authorize access="isAnonymous()">
		<div class="hotels"> 
 			<c:forEach items="${hotels}" var="hotel">
 			<div id="hotel" class="buttonElement" url="roomsByHotel.html?id=${hotel.id}"  onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'">
 			<img src="img/hotel.png">  ${hotel.name};<br>
 			<img src="img/country.png"> ${hotel.country}<br>
 			<img src="img/city.png"> ${hotel.city}<br>
 			</div>
 			<a href="/HotelBookingService/hotelInfo.html?id=${hotel.id}" class="ajax">Hotel Info</a>
 			</c:forEach>
 	</div>
 </sec:authorize>
 </div>