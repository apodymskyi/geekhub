<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table border="1" style="width:100%;">
    <tr>
        <th>First Name, Last Name</th>
        <th>E-Mail</th>
        <th>Phone</th>
        <th>Role</th>
        <th>Actions</th>
    </tr>

<c:forEach items="${users}" var="user">
    <tr>
        <td>${user.firstName} ${user.lastName}</td>
        <td>${user.email}</td>
        <td>${user.phone}</td>
        <td>${user.role}</td>
        <td><a href="./loadUser.action?id=${user.id}" class="ajax">Edit</a><br>
        <a href="./deleteUser.action?id=${user.id}" class="ajax">Delete</a></td>

    </tr>
</c:forEach>

</table>