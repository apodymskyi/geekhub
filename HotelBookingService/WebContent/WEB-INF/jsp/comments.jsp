<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div id="commentHeader"> <img src="img/commentHeaderSign.png" width="12px" height="12px"> Comments </div>
<div id="commentStatus"> ${commentStatus} </div>
<br>
	<c:forEach items="${comments}" var="comment">
		<div id="comment${comment.id}" class="comment">
		${comment.owner.firstName}: ${comment.text}  <span id="commentPostDate"><fmt:formatDate value="${comment.postDate}" pattern="dd-MM-yyyy"/></span>
		 <sec:authorize access="hasRole('ROLE_USER')">
		 	<c:if test="${loggedUserID == comment.owner.id}">
				<a href="/HotelBookingService/deleteComment.html?comment.id=${comment.id}" class="ajaxdivupdate" divtoupdate="commentStatus" onclick="deletethiscomment(${comment.id})">Delete</a>
			</c:if>
		</sec:authorize>
		<sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_HOTEL')">
			<a href="/HotelBookingService/deleteCommentByMgr.html?comment.id=${comment.id}" class="ajaxdivupdate" divtoupdate="commentStatus" onclick="deletethiscomment(${comment.id})">Delete</a>
		</sec:authorize>
		</div>
	</c:forEach>




