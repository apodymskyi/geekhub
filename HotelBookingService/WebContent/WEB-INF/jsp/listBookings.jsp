<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
td.last {
    width: 1px;
    white-space: nowrap;
}
</style>
<table border="1px" style="width: 100%;">
	<tr>
		<th>DateIn</th>
		<th>DateOut</th>
		<th>Hotel</th>
		<th><img src="img/roomNumber.png"></th>
		<th>Status</th>
		<th><img src="img/visa-mc.jpg"></th>
		<th>Action</th>
	</tr>
	<c:forEach items="${bookings}" var="booking">
		<tr>
			<td><fmt:formatDate value="${booking.dateIn}" pattern="dd/MM/yy" /></td>
			<td><fmt:formatDate value="${booking.dateOut}" pattern="dd/MM/yy" /></td>
			<td>${booking.hotel.name}</td>
			<td>${booking.room.roomNumber}</td>
			<td style="size:10px;">${booking.status}</td>
			<td>
			<c:set var="payment" value="${booking.payment}"/>
			<c:if test="${not empty payment}" >
			${booking.payment.cost}$
			</c:if>
			</td>
			<td><a href="./deleteBooking.html?id=${booking.id}" class="ajax">Delete</a></td>
		</tr>
	</c:forEach>
</table>