<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript" src="js/datePicker.js"> </script> 
<script type="text/javascript" src="js/valid.free.rooms.list.js"> </script>

<script type="text/javascript" src="js/location.js"> </script>

<link rel="stylesheet" type="text/css" href="css/error.css">
 
<div class="form">
<form action="/HotelBookingService/listFreeRooms.html" id="Form" class="ajaxdivupdate" divToUpdate="rooms">
<div id="errorContainer">
	<p style="color:red;">Please correct the following errors and try again:</p>
    <ul />
	</div>
	Room search:
	<table style="width:100%;">
		<tr>
		<td>Price:</td>
			<td> <input type="text" pattern="\d+" value="${price}" name="price" id="price" class="field"></td>
			<td>Occupancy:</td>
			<td><input type="number" step="1" min="1" value="1" name="occupancy" id="occupancy" class="field"></td>		
		</tr>
		<tr>
			<br><div id="countries"></div> <span id="cities"></span>
		</tr>
		<tr>
    	<td>	  
			Type:</td> <td><select name="type" class="field">
				<option value="">Any</option>
				<option value="LUX">LUX</option>
				<option value="HALF_LUX">HALF_LUX</option>
				<option value="STANDART">STANDART</option>
				<option value="ECONOMY">ECONOMY</option>
				</select></td>		
		</tr>
		<tr>		
			<td>DateIN:</td> <td><input type="text" value="${dateIn}" name="dateIn" class="datepicker" id="dateIn" readonly="readonly"></td>
			<td>DateOut:</td> <td><input type="text" value="${dateOut}" name="dateOut" class="datepicker" id="dateOut" readonly="readonly"></td>
		</tr>
		<tr>
			<td>Show:</td><td colspan="3"><input type="number" step="10" min="10" max="50" value="10" name="limit" class="field"></td>	
		</tr>
	</table>
		<input type="hidden" name="status" value="CONFIRMED">	
	<input type="submit" value="Filtrate">
	<input name="reset" type="reset" value="Reset"/>
	
</form>
</div>
<div class="rooms" id="rooms">