<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
<title>Login Page</title>
</head>
<style>
#form {
	Width: 262px;
	Height: 38px;
	background: #FDF5E6;
	border-radius: 20px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 5px;
	-khtml-border-radius: 10px;
	vertical-align: middle;
	border: 1px black solid;
	text-align: center;
	display: table-cell;
}
</style>
<body>
	<div id="form">
		<h1 align=center>Login</h1>
		<form name='f' action="<c:url value='j_spring_security_check' />" method='POST'>
			<div>
				E-MAIL:&nbsp;<input type="text" name="j_username" />
			</div>
			<div>
				Password:&nbsp;<input type='password' name='j_password' />
			</div>
			<div>
				<input name="submit" type="submit" value="submit" /> 
				<input name="reset" type="reset" value="Reset"/>
			</div>
		</form>
	</div>
</body>
</html>