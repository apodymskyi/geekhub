<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="js/hotel.js"> </script>
<style>
.error {
    float: none;
    color: red;
    padding-left: .5em;
    vertical-align: top;
    display: block;
}
.field {
	width: 300px;
	background-color: white;
	align: center;
}
</style>
<form id="addHotel" action="/HotelBookingService/saveHotel.html" class="ajax">
	<H2 align="center">ADD HOTEL</H2>
	<table style="width:100%">
		<tr>
			<td>Name</td>
			<td><input type="text" value="${hotel.name}" name="name" class="field"></td>
		</tr>
		<tr>
			<td>Description</td>
			<td>
				<textarea rows="5" value="${hotel.description}" name="description" class="field"></textarea>
			</td>
		</tr>
		<tr>
			<td>Country</td>
			<td><input type="text" value="${hotel.country}" name="country" class="field"></td>
		</tr>
		<tr>
			<td>City</td>
			<td><input type="text" value="${hotel.city}" name="city" class="field"></td>
		</tr>
		<tr>
			<td>Owner</td>
			<td>
			<input type="hidden" name="owner.id" value="${loggedUser.id}">
			${loggedUser.firstName} ${loggedUser.lastName}
			</td>
		</tr>
	</table>
	<input type="hidden" name="id" value="${hotel.id}"> 
	<div align=center>
		<input type="submit" value="Save" id="submit" style="width: 50%; font-weight:bold; font-size: 20px">
	</div>
</form>
	