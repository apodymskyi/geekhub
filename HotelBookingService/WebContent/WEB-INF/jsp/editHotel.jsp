<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="Form" action="/HotelBookingService/saveHotel.html" class="ajax">
	<H2 style="width: 415px;" align="center">ADD HOTEL</H2>
	<table>
		<tr>
			<td>Name</td>
			<td><input type="text" value="${hotel.name}" name="name"
				style="width: 300px;"></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><input type="text" value="${hotel.description}"
				name="description" style="width: 300px; height: 117px"></td>
		</tr>
		<tr>
			<td>Country</td>
			<td><input type="text" value="${hotel.country}" name="country"
				style="width: 300px;"></td>
		</tr>
		<tr>
			<td>City</td>
			<td><input type="text" value="${hotel.city}" name="city"
				style="width: 300px;"></td>
		</tr>
		<tr>
			<td>Owner</td>
			<td>
			<input type="hidden" name="owner.id" value="${hotel.owner.id}">
			${hotel.owner.firstName} ${hotel.owner.lastName}
				<!--  <select name="owner.id">
						<c:forEach items="${users}" var="user">
							<option value="${user.id}">${user.firstName}
								${user.lastName} (${user.id})</option>
						</c:forEach>  
				</select> -->
			</td>
		</tr>
	</table>
	<input type="hidden" name="id" value="${hotel.id}"> <input
		type="submit" value="Save" id="submit" style="width: 188px">
</form>
	