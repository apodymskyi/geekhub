<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script type="text/javascript" src="js/location.js"> </script>
 

<div id="hotelForm">
<form action="/HotelBookingService/hotels.html" id="Form" class="ajaxdivupdate" divToUpdate="hotelsView">
	<span id="countries"></span> <span id="cities"></span>
	<br>
	
	<input type="submit" value="Filtrate">
</form>
</div>


<div id="hotelsViewAdmin">
<sec:authorize access="hasRole('ROLE_ADMIN')">
<table border="1" style="width:100%;">
    <tr>
        <th>Hotel Name</th>
        <th>Country</th>
        <th>City</th>
        <th>Details</th>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
        <th colspan="3">Administrator View</th>
        </sec:authorize>
    </tr>

<c:forEach items="${hotels}" var="hotel">
    <tr>
        <td>${hotel.name}</td>
        <td>${hotel.country}</td>
        <td>${hotel.city}</td>
		<td><a href="/HotelBookingService/hotelInfo.html?id=${hotel.id}" class="ajax">Hotel Info</a></td>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
        <td>${hotel.owner.firstName} ${hotel.owner.lastName}</td>
        <td><a href="/HotelBookingService/loadHotel.html?id=${hotel.id}" class="ajax">Edit</a></td>
        <td><a href="/HotelBookingService/deleteHotel.html?id=${hotel.id}" class="ajax">Delete</a></td>
		</sec:authorize>
    </tr>
</c:forEach>

</table>
</sec:authorize>
</div>

<div id="hotelsView"></div>