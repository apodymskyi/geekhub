<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<center>
	<h2>${hotel}'s room list</h2>
</center>
<script type="text/javascript" src="js/valid.fileformat.js"></script>
<table border="1" style="width: 100%">
	<tr>
		<th><img src="img/roomNumber.png" title="Room Number"></th>
		<th><img src="img/occupancy.png" title="Occupancy"></th>
		<th><img src="img/roomType.png" title="Room Type"></th>
		<th><img src="img/price.png" title="Price per day"></th>
		<th></th>
		<sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_HOTEL')">
			<th colspan="2">Manage</th>
		</sec:authorize>
	</tr>

	<c:forEach items="${rooms}" var="room">
		<tr>
			<td>${room.roomNumber}</td>
			<td>${room.occupancy}</td>
			<td>${room.type}</td>
			<td>${room.price}</td>
			<td><a href="/HotelBookingService/roomInfo.html?id=${room.id}" class="ajax">Info</a></td>
			<sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_HOTEL')">
				<td><a href="/HotelBookingService/loadRoom.html?id=${room.id}" class="ajax">Edit</a></td>
				<td><a href="/HotelBookingService/deleteRoom.html?id=${room.id}" class="ajax">Delete</a></td>
			</sec:authorize>
		</tr>
		<sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_HOTEL')">
			<tr>
				<td colspan="5">
					<form id="upload" enctype="multipart/form-data" method="POST" 
					action="./uploadImage.html?scope=room&id=${room.id}"   target="status">
						Add photo <input onchange="Checkfiles()" type="file" name="file" id="file" accept="image/*, image/jpeg" > 
						<input type="submit" value="Upload">
					</form>
				</td>
				<td colspan="2"><a href="./managePhoto.html?scope=room&id=${room.id}" class="ajax">Manage Photos</a></td>
			</tr>
		</sec:authorize>
	</c:forEach>
</table>
<br>
<iframe name="status" height="50" allowTransparency frameborder="0"></iframe>
