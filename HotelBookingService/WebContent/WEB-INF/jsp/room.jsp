<script type="text/javascript" src="js/valid.new.room.js"> </script>
<style>
.error {
    float: none;
    color: red;
    padding-left: .5em;
    vertical-align: top;
    display: block;
}
.field {
	width: 300px;
	background-color: white;
	align: center;
}
</style>
<form id="addRoom" action="./saveRoom.html" class="ajax">
<table>
		<tr>
			<td>Number</td>
			<td><input type="text" value="${room.roomNumber}" name="roomNumber" class="field"></td>
		</tr>
		<tr>
			<td>DESCRIPTION</td>
			<td>
				<textarea rows="5" value="${room.description}" name="description" class="field"></textarea>
			</td>
		</tr>
		<tr>
			<td>OCCUPANCY</td>
			<td><input type="text" value="${room.occupancy}" name="occupancy" class="field"></td>
		</tr>
		<tr>
			<td>TYPE</td>
			<td><select name="type" class="field">
			<option value="${room.type}">${room.type}</option>
			<option value="LUX">LUX</option>
			<option value="HALF_LUX">HALF LUX</option>
			<option value="STANDART">STANDART</option>
			<option value="ECONOMY">ECONOMY</option>
			</select></td>
		</tr>
		<tr>
			<td>Price</td>
			<td><input type="text" value="${room.price}" name="price" class="field"></td>
		</tr>
		
	</table>
	<input type="hidden" name="id" value="${room.id}">
	<input type="hidden" name="hotel.id" value="${hotelId}">
	<div align=center>
	<input type="submit" value="Save" id="submit" style="width: 50%; font-weight:bold; font-size: 20px">
	</div>
</form>