<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="js/datePicker.js"> </script>
<script type="text/javascript" src="js/valid.room.free.js"> </script>

<form action="./saveEditedBooking.html" id="Form" class="ajax">
    <br>
	<h1 align=center>Edit Booking</h1>
	<br>
	<br>
	Owner
    <input type="hidden" name="owner.id" value="${booking.owner.id}"> ${booking.owner.firstName} ${booking.owner.lastName}
    <br>
	
	Hotel: ${booking.hotel.name}
	<br>	
	Room
	<select name="room.id">
        <option value="${booking.room.id}">${booking.room.roomNumber} (id: ${booking.room.id})</option>
    <c:forEach items="${rooms}" var="room">
        <option value="${room.id}">${room.roomNumber} (id: ${room.id})</option>
    </c:forEach>
    </select><br>
	
	DateIN: <input type="text" value="<fmt:formatDate value="${booking.dateIn}" pattern="yyyy-MM-dd"/>" name="dateIn" readonly="readonly"><br>
	
	DateIN: <input type="text" value="<fmt:formatDate value="${booking.dateOut}" pattern="yyyy-MM-dd"/>" name="dateOut" readonly="readonly"><br>
	
	Booking Status  
	<select name="status">
	   <option value="CONFIRMED">CONFIRMED</option>
        <option value="NOT_CONFIRMED">NOT_CONFIRMED</option>
    </select><br>
	    
	<input type="hidden" name="hotel.id" value="${booking.hotel.id}">
	<input type="hidden" name="id" value="${booking.id}">
	<c:set var="payment" value="${booking.payment}" /> 
	<c:if test="${not empty payment}">
		<input type="hidden" name="payment.id" value="${booking.payment.id}">
	</c:if>
	<input type="submit" value="Save">
</form>


