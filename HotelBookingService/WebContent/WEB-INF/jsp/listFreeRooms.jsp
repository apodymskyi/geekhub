<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript" src="js/datePicker.js"> </script> 
<script type="text/javascript" src="js/valid.free.rooms.list.js"> </script>

Rooms found: <c:out value="${roomsFound}"/>

	<c:forEach items="${rooms}" var="room">
    <div class="buttonElement" id="room" url="addBooking.html?hotel_id=${room.hotel.id}&room_id=${room.id}&dateIn=${dateIn}&dateOut=${dateOut}" onmouseover="this.style.color='blue'" onmouseout="this.style.color='black'">
    	<img src="img/roomNumber.png"> 	${room.roomNumber}<br> 
    	<img src="img/hotel.png"> ${room.hotel.name}<br>
        <img src="img/country.png">  ${room.hotel.country}  
        <img src="img/city.png"> ${room.hotel.city}
        <img src="img/occupancy.png">${room.occupancy}
        <img src="img/roomType.png">${room.type}
		<img src="img/price.png">${room.price}
		<!--  ${room.description} -->
		<!--	<a href="/HotelBookingService/addBooking.html?hotel_id=${room.hotel.id}&room_id=${room.id}&dateIn=${dateIn}&dateOut=${dateOut}" class="ajax">Book</a>  -->
			<br>
	</div>
	 </c:forEach>
