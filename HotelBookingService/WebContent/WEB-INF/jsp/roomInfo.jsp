<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!-- CommentsSupport -->
	<script type="text/javascript" src="js/comments.js"> </script> 
	<script type="text/javascript">
		$(document).ready(function() {
			showRoomComments();
		});
	</script>

<table border="1px" style="width: 100%;">
	<tr>
		<td style="color: blue; text-align: center; font-size: 30px"
			colspan="3"><strong> Hotel:${room.hotel.name} <br>
				Room <img src="img/roomNumber.png"> ${room.roomNumber} </strong></td>
	</tr>
	<tr>
		<td><strong><img src="img/occupancy.png"></strong> ${room.occupancy}</td>
		<td><strong><img src="img/roomType.png"></strong> ${room.type}</td>
		<td><strong><img src="img/price.png"></strong> ${room.price}</td>
	</tr>
	<tr>
		<td colspan="3"><strong>Description:</strong>${room.description}</td>
	</tr>
</table>
<div id="photos" style="height: 130px; overflow-x: auto;">
	<c:forEach items="${photos}" var="id">
		<a href="./getPhoto.html?id=${id}" target="_blank" rel="lightbox[room]" title="room">
			<img src="./getPhoto.html?id=${id}" style="max-height: 100%; max-width: 100%"></a>

	</c:forEach>
</div>


<form action="/HotelBookingService/addRoomComment.html" id="commentForm" class="ajaxdivupdate" divToUpdate="comments">

	
	<input type="hidden" name="room.id" value="${room.id}">
	<input type="hidden" name="id" value="${comment.id}">
	Show <input type="number" step="10" min="10" max="100" value="10" name="showResults"  onchange="showRoomComments()" class="ajaxdivupdate" divToUpdate="comments"> comments
	<sec:authorize access="isAuthenticated()">
		<textarea rows="2" value="${comment.text}" name="text" class="field"  maxlength="100" style='width:98%; resize:none;'></textarea>
		<input type="submit" value="Comment" id="submit">
	</sec:authorize>
</form>

<div id="comments"></div>