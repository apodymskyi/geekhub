$(document).ready(function() {
	$("#Form").validate({
		rules : {
			firstName : {
				required : true,
				minlength : 2
			},

			lastName : {
				required : true,
				minlength : 2
			},

			password : {
				required : true,
				minlength : 8,
				maxlength : 16

			},

			email : {
				required : true,
				email : true
			},
			phone : {
				required : true,
				digits : true,
				minlength : 6,
				maxlength : 12
			},
			role : 'required'

		},

		messages : {
			firstName : {
				required : "Please specify your FirstName",
				minlength : "Name must be 2 or more letters"
			},
			lastName : {
				required : "Please specify your LastName",
				minlength : "Name must be 2 or more letters"
			},
			email : {
				required : "E-mail is required field!"
			},
			password : {
				minlength : "Password must be 8-16 symbols",
				maxlength : "Password must be 8-16 symbols",
				required : "Please specify password"
			},
			phone : {
				required : "Please enter yor phone number",
				maxlength : "6-12 digits required",
				minlength : "6-12 digits required"

			}
		}
	});
});