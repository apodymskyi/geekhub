function checkLogin() {
	var email = document.getElementById('email').value;

	$.ajax({
		url : "check.html?email=" + email,
		type : "GET",
		dataType : "html",
		success : function(msg) {
			$('#check').removeClass();
			if (msg == "OK") {
				$('#check').addClass("green");
				$("#submit").removeAttr("disabled");
			} else {
				$('#check').addClass("error");
				$("#submit").attr("disabled", "disabled");
			}

			$("#check").html(msg);
		}
	});
}