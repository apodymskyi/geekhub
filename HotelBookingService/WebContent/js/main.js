$(document).ready(function() {

/* Make button-class URL param  to use AJAX */
$(".button").click(function () {
				var link = $(this).attr("url");
				$("#column-center").html('<img src="img/loading.gif">');
				$.ajax({
					url: link,
					type: "GET",
					dataType: "html",
					success: function(response) {
                		$("#column-center").html(response);
					}
				});
			});

/*Make #column-center's buttonElement to use AJAX*/
$("#column-center").on("click", ".buttonElement", function(e){
		var link = $(this).attr("url");
		$.ajax({
			url: link,
			type: "GET",
			dataType: "html",
			success: function(response) {
        		$("#column-center").html(response);
			}
		});
	});

/*Make #column-center's  <a> to use AJAX*/
$("#column-center").on("click", "a.ajax", function(e){
			var link= $(this);	
			var target = $(this).attr("target");	
			if ( target!="_blank")
				{						
					e.preventDefault();
					$.ajax({
						url: link.attr('href'),
						type: "GET",
						dataType: "html",
						success: function(response) {
							$("#column-center").html(response);
						}			
					});
				}
			});
/*Make #column-center's  <a> to use AJAX to update target div*/
$("#column-center").on("click", "a.ajaxdivupdate", function(e){
	var link= $(this);	
	var target = $(this).attr("target");
	var divToUpdateName = "#"+$(this).attr('divToUpdate');
	$(divToUpdateName).html('<img src="img/loading.gif">');
	if ( target!="_blank")
		{						
			e.preventDefault();
			$.ajax({
				url: link.attr('href'),
				type: "GET",
				dataType: "html",
				success: function(response) {
					$(divToUpdateName).html(response);
				}			
			});
		}
	});
/*Upload photo*/ 
$("#column-center").on("submit", "#upload",function(e) {
	alert("Photo is uploading");	
	});

/*Make #column-center's  forms to use AJAX*/
$("#column-center").on("submit", "form.ajax",function(e) {
		e.preventDefault();
		var formUrl = $("form").attr('action');
		var formData= $("form").serialize();
		$("#column-center").html('<img src="img/loading.gif">');
		$.ajax({
			url: (formUrl+'?'+formData),
			type: "POST",
			dataType: "html",
			success: function(response) {
				$("#column-center").html(response);
				}			
		});
	});
/*Make #column-center's  forms to use AJAX to update target div*/
$("#column-center").on("submit", "form.ajaxdivupdate",function(e) {
	e.preventDefault();
	var formUrl = $("form").attr('action');
	var formData= $("form").serialize();
	var divToUpdateName = "#"+$("form").attr('divToUpdate');
	$(divToUpdateName).html('<img src="img/loading.gif">');
	$.ajax({
		url: (formUrl+'?'+formData),
		type: "POST",
		dataType: "html",
		success: function(response) {
			$(divToUpdateName).html(response);
			}			
	});
	
});





/*Make registration  form to use AJAX*/
$("#column-right").on("submit", "form",function(e) {
					var formUrl = $("form").attr('action');
            		var formData= $("form").serialize();
            		$.ajax({
   						url: (formUrl+'?'+formData),
   						type: "GET",
   						dataType: "html",
   						success: function(response) {
   							$("#column-center").html(response);
   						},
						error: function(ans) {
							$("#column-center").html(ans);
						},						
   				 });
     });


});