function showcountries(){
	$("#countries").html('<img src="img/loading.gif">Loading countries');
	$.ajax({
		url: 'countries.html',
		type: "GET",
		dataType: "html",
		success: function(response) {
			$("#countries").html(response);
			}			
	});
};

function showcities(){
	
	$("#cities").html('<img src="img/loading.gif">Loading cities');
	var country =  $("select[name='country']").val();
	$.ajax({
		url: 'cities.html?country='+country,
		type: "GET",
		dataType: "html",
		success: function(response) {
			$("#cities").html(response);
			}			
	});
};

$(document).ready(function(){
	showcountries();	
});