$.validator.addMethod(
    "fmtDate",
    function(value, element) {
        return value.match(/^\d\d\d\d?\-\d\d?\-\d\d$/);
    },
    "Please enter a date in the format yyyy-mm-dd."
);

$.validator.addMethod(
		"greaterThan",
		function(value, element, params) {
		    if (!/Invalid|NaN/.test(new Date(value))) {
		        return new Date(value) > new Date($(params).val());
		    }
		    return isNaN(value) && isNaN($(params).val()) 
		        || (Number(value) > Number($(params).val())); 
		  },
		"DateOut must be greather than DateIn"
);

$(document).ready(function() {
	$("#Form").validate({
		rules : {
			dateOut : {
				required : true,
				fmtDate : true,
				greaterThan: "#dateIn"
			},
			dateIn : {
				required : true,
				fmtDate : true,
			}
		},

		messages : {
			dateIn : {
				required : "DateIn is required"
			},
			dateOut : {
				required : "DateOut is required"
			}
		},
		errorContainer: $('#errorContainer'),
	    errorLabelContainer: $('#errorContainer ul'),
	    wrapper: 'li'
	});
});