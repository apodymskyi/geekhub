$(function() {
		$( ".datepicker" ).datepicker({
			dateFormat:'yy-mm-dd',
			autoSize: true,
			showAnim:"slide",showButtonPanel:true,
			changeYear:true,
			buttonImage:"img/calendar.gif", 
			showOn:"button",
			buttonImageOnly:true,
			minDate: 0
			});
 	 });