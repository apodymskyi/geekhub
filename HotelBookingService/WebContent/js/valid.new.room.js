$(document).ready(function() {
	$("#Form").validate({
		rules : {
			roomNumber : {
				required : true,
				digits : true
			},

			description : {
				required : true,
				minlength : 20
			},

			occupancy : {
				required : true,
				digits : true
			},

			type : 'required',

			price : {
				required : true,
				digits : true
			},
		},

		messages : {
			roomNumber : {
				required : "Please specify your Room Number",
				digits : "only digits"
			},
			description : {
				required : "Please specify Description",
				minlength : "Description must be 20 or more letters"
			},
			occupancy : {
				required : "Please specify occupancy!"
			},
			type : {
				required : "Please select room type"
			},
			price : {
				required : "Please specify room price",
			}
		}
	});
});