function calculateBookingCost(){
	var roomPrice = document.getElementById('roomPrice').value;
	var dateIn = document.getElementById('dateIn').value;
	var dateOut = document.getElementById('dateOut').value;
	$("#cost").html('<img src="img/loading.gif">');	
	if (dateIn !="" && dateOut !=""){
		$.ajax({
			url : "calculateBookingCost.html?roomPrice=" + roomPrice +"&dateIn="+dateIn + "&dateOut="+dateOut,
			type : "GET",
			dataType : "html",
			success : function(cost) {
				$("#cost").html("Total: "+cost);
		}
	});
	};
};

$(document).ready(function(){
		calculateBookingCost();
});

