function showRoomComments(){
	var roomID =  $("input[name='room.id'][type='hidden']").val();
	var showResults =  $("input[name='showResults']").val();
	$("#comments").html('<img src="img/loading.gif">');
	$.ajax({
		url: 'listRoomComments.html?room.id='+roomID+'&showResults='+showResults,
		type: "GET",
		dataType: "html",
		success: function(response) {
			$("#comments").html(response);
			}			
	});
};

function showHotelComments(){
	var hotelID =  $("input[name='hotel.id'][type='hidden']").val();
	var showResults =  $("input[name='showResults']").val();
	$("#comments").html('<img src="img/loading.gif">');
	$.ajax({
		url: 'listHotelComments.html?hotel.id='+hotelID+'&showResults='+showResults,
		type: "GET",
		dataType: "html",
		success: function(response) {
			$("#comments").html(response);
			}			
	});
};

function deletethiscomment(number){
	$("#comment"+number).fadeOut();
};

