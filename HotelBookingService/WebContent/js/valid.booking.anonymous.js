$.validator.addMethod(
    "fmtDate",
    function(value, element) {
        return value.match(/^\d\d\d\d?\-\d\d?\-\d\d$/);
    },
    "Please enter a date in the format yyyy-mm-dd."
);

$.validator.addMethod(
		"greaterThan",
		function(value, element, params) {
		    if (!/Invalid|NaN/.test(new Date(value))) {
		        return new Date(value) > new Date($(params).val());
		    }
		    return isNaN(value) && isNaN($(params).val()) 
		        || (Number(value) > Number($(params).val())); 
		  },
		"DateOut must be greather than DateIn"
);


$(document).ready(function() {
	$("#Form").validate({
		rules : {
			dateOut : {
				required : true,
				fmtDate : true,
				greaterThan: "#dateIn"
			},
			dateIn : {
				required : true,
				fmtDate : true,
			},
			firstName : {
				required : true,
				minlength : 2
			},

			lastName : {
				required : true,
				minlength : 2
			},

			password : {
				required : true,
				minlength : 8,
				maxlength : 16

			},

			email : {
				required : true,
				email : true
			},
			phone : {
				required : true,
				digits : true,
				minlength : 12,
				maxlength : 12
			},
			role : 'required'
		},
		messages : {
			dateIn : {
				required : "DateIn is required"
			},
			dateOut : {
				required : "DateOut is required"
			},
			firstName : {
				required : "Please specify your FirstName",
				minlength : "Name must be 2 or more letters"
			},
			lastName : {
				required : "Please specify your LastName",
				minlength : "Name must be 2 or more letters"
			},
			email : {
				required : "E-mail is required field!"
			},
			password : {
				minlength : "Password must be 8-16 symbols",
				maxlength : "Password must be 8-16 symbols",
				required : "Please specify password"
			},
			phone : {
				required : "Please enter yor phone number",
				maxlength : "12 digits Phone required",
				minlength : "12 digits Phone required"

			}
		},
		errorContainer: $('#errorContainer'),
	    errorLabelContainer: $('#errorContainer ul'),
	    wrapper: 'li'
	});
});