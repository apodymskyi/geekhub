$.validator.addMethod(
		    "fmtCardDate",
		    function(value, element) {
		        return value.match(/^\d\d?\/\d\d\d\d$/);
		    },
		    "Please enter your card expiration date in the format MM/YYYY."
		);
	

$(document).ready(function() {
		
	$("#braintree-payment-form").validate({
		rules : {
			number : {
				required : true,
				digits : true,
				minlength : 12,
				maxlength : 19
			},

			cvv : {
				required : true,
				digits : true,
				minlength : 3,
				maxlength : 4
			},

			date : {
				required : true,
				fmtCardDate: true
			},
		},

		messages : {
			number : {
				required : "Please specify your Credit Card number",
				digits : "only digits",
				minlength : "Credit Card number must contain 12-19 digits",
				maxlength : "Credit Card number must contain 12-19 digits"
			},
			cvv : {
				required : "Please specify your CVV",
				digits : "only digits",
				minlength : "CVV must contain 3-4 digits",
				maxlength : "CVV must contain 3-4 digits"
			},
			date : {
				required : "Please specify your cards' expairation date",
			}
		},
		errorContainer: $('#errorContainer'),
	    errorLabelContainer: $('#errorContainer ul'),
	    wrapper: 'li'
	});
});