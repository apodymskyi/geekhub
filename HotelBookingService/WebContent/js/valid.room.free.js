function checkRoomToBeFree(){
	var roomId = document.getElementById('roomId').value;
	var dateIn = document.getElementById('dateIn').value;
	var dateOut = document.getElementById('dateOut').value;
	if (dateIn !="" && dateOut !=""){
		$("#submit").attr("disabled", "disabled");
		$.ajax({
			url : "checkRoomToBeFree.html?roomId=" + roomId +"&dateIn="+dateIn + "&dateOut="+dateOut+"&safeCheck="+true,
			type : "GET",
			dataType : "html",
			success : function(msg) {
				$('#checkRoomFree').removeClass();
				if (msg == "free") {
						$('#checkRoomFree').css('color', 'green');
						$("#submit").removeAttr("disabled");
						calculateBookingCost();
				} else {
						$("#submit").attr("disabled", "disabled");
						$("#pay").attr("disabled", "disabled");
						$('#checkRoomFree').css('color', 'red');
						$('#cost').html("");
				}
			$("#checkRoomFree").html("Room is "+msg);
		}
	});
	};
}