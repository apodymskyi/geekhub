$(document).ready(function() {
	$("#addHotel").validate({
		rules : {
			name : {
				required : true,
				minlength : 2
			},

			description : {
				required : true,
				minlength : 20
			},

			country : {
				required : true,
				minlength : 2
			},

			city : {
				required : true,
				minlength : 2
			}
		},

		messages : {
			name : {
				required : "Please specify your Hotel Name",
				minlength : "Name must be 2 or more letters"
			},
			description : {
				required : "Please specify Description",
				minlength : "Description must be 20 or more letters"
			},
			country : {
				required : "Please specify country!"
			},
			city : {
				required : "Please specify city!"
			}
		}
	});
});