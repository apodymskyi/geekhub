package util;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtil {

	private StringUtil(){
	}

	public static boolean strNotNullOrEmpty(String str) {
		if (str == (null) || str.equals(""))
			return false;
		else
			return true;
	}

	public static Date stringToDate(String dateAsString) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		Date date = null;
		try {
			date = dateFormat.parse(dateAsString);
		} catch (ParseException e) {
			//nothing
		}
		return date;
	}

	public static String dateToString(Date date) {
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(date);
	}
}
