package com.hotelbooking.services;

import java.util.List;


import com.hotelbooking.beans.Room;
import com.hotelbooking.beans.SearchCriteria;

public interface RoomDAO {

	public void saveRoom(Room room);
	public void deleteRoom(Integer id);
	public void deleteRoomByHotelId(Integer id);
	public List<Room> getRoomListByCriteria(SearchCriteria criteria);
	public List<Room> getRoomListByHotel(Integer hotelId);
	public List<Room> getRoomList();
	public Room getRoomById(Integer roomID);
	
}
