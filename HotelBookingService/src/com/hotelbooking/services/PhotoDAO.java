package com.hotelbooking.services;

import java.util.List;

import com.hotelbooking.beans.Photo;

public interface PhotoDAO {

	public void savePhoto(Photo photo);
    public List<Integer> listByHotelId(Integer id);
    public List<Integer> listByRoomId(Integer id);
    public Photo getPhotoById(Integer id);
    public void deletePhoto(Integer id);
    public void deletePhotoByHotelId(Integer id);
    public void deletePhotoByRoomId(Integer id);
    public List<Photo> manageByHotelId(Integer id);
    public List<Photo> manageByRoomId(Integer id);
    
}
