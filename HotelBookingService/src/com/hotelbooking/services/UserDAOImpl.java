package com.hotelbooking.services;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hotelbooking.beans.User;

//Author: Ihor Chanzhar

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {

	@Autowired
	SessionFactory sessionFactory;

	public User getUserById(Integer id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsersList() {
		return sessionFactory.getCurrentSession().createQuery("from User")
				.list();
	}

	public void saveUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	public void deleteUser(Integer id) {
		User user = getUserById(id);
		if (null != user)
			sessionFactory.getCurrentSession().delete(user);
	}

	public boolean checkUser(String email) {
		Query q = sessionFactory.getCurrentSession().createQuery("from User where email=:email");
		q.setString("email", email);
		boolean result = q.uniqueResult() != null; 
		return result;
	}

	public User getUserByEmail(String email) {
		Query q = sessionFactory.getCurrentSession().createQuery("from User where email=:email");
		q.setString("email", email);
		return (User) q.uniqueResult();
	}


	public User getLoggedUser() {
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		if(!"anonymousUser".equals(name)){
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(null!=user)
			return user;
		}
		return null;
	}

}
