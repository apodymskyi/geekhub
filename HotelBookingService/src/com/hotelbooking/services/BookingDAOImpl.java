package com.hotelbooking.services;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import util.StringUtil;

import com.hotelbooking.beans.Booking;
import com.hotelbooking.beans.Room;
import com.hotelbooking.beans.SearchCriteria;

@Repository
@Transactional
public class BookingDAOImpl implements BookingDAO {

	@Autowired SessionFactory sessionFactory;

	public void saveBooking(Booking booking) {
		sessionFactory.getCurrentSession().saveOrUpdate(booking);
	}

	@SuppressWarnings("unchecked")
	public List<Booking> getBookingList() {
		return sessionFactory.getCurrentSession().createQuery("from Booking").list();
	}

	@SuppressWarnings("unchecked")
	public List<Booking> getBookingListByHotel(int id, int show) {
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Booking.class);
		crit.add(Restrictions.eq("hotel.id", id));
		crit.addOrder(Order.desc("dateIn"));
		crit.setMaxResults(show);
		return crit.list();
	}
	
	public Booking getBookingById(Integer id) {
		return (Booking) sessionFactory.getCurrentSession().get(Booking.class, id);
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Booking> getBookingByCriteria(SearchCriteria searchCriteria) {
		
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Booking.class);
		crit.setFetchMode("room", FetchMode.LAZY);
		
		if (searchCriteria.getHotelId()!=null)
			crit.add(Restrictions.eq("hotel.id", searchCriteria.getHotelId()));
		if (searchCriteria.getDateIn()!=null)
			crit.add(Restrictions.eq("dateIn", searchCriteria.getDateIn()));
		
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Room> getRoomsByCriteria(SearchCriteria searchCriteria) {

		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Room.class);
		
//		ROOM filter by Occupancy,Type,PRICE
		if (searchCriteria.getRoomOccupancy()!=null)
			crit.add(Restrictions.ge("occupancy", searchCriteria.getRoomOccupancy()));
		
		if (searchCriteria.getRoomType()!=null)
			crit.add(Restrictions.eq("type", searchCriteria.getRoomType()));
		
		if (searchCriteria.getRoomPrice()!=null)
			crit.add(Restrictions.le("price", searchCriteria.getRoomPrice()));
		
//		ROOM filter by Hotel, Country, City
		crit.createAlias("hotel","h");	

		if (searchCriteria.getHotelId()!=null)
			crit.add(Restrictions.eq("hotel.id", searchCriteria.getHotelId()));
		
		if (StringUtil.strNotNullOrEmpty(searchCriteria.getCountry())){
			crit.add(Restrictions.eq("h.country", searchCriteria.getCountry()));
			if (StringUtil.strNotNullOrEmpty(searchCriteria.getCity()))
				crit.add(Restrictions.eq("h.city", searchCriteria.getCity()));
		}
		
//		ROOM filter by booking-free status (dateIn,dateOut)	
	
		if ((null!=searchCriteria.getDateIn())&&((null!=searchCriteria.getDateOut()))){
			
			DetachedCriteria dc = DetachedCriteria.forClass(Room.class);
			dc.setProjection(org.hibernate.criterion.Property.forName("id"));
			dc.createAlias("bookings", "b");
			Disjunction or = Restrictions.disjunction();
			
			or.add(Restrictions.between("b.dateIn", searchCriteria.getDateIn(), searchCriteria.getDateOut()));
			or.add(Restrictions.between("b.dateOut", searchCriteria.getDateIn(), searchCriteria.getDateOut()));
			or.add(Restrictions.conjunction()
							.add(Restrictions.le("b.dateIn", searchCriteria.getDateIn()))
							.add(Restrictions.ge("b.dateOut", searchCriteria.getDateOut())));
			
//			if (null!=searchCriteria.getBookingStatus())
//				dc.add(Restrictions.conjunction()
//						.add(Restrictions.eq("b.status", searchCriteria.getBookingStatus()))
//						.add(or));
//			else
			dc.add(or);
			
			crit.add(org.hibernate.criterion.Property.forName("id").notIn(dc));
			

		}
		return crit.list();
	}
	
	public void deleteBooking(Integer id) {
		Booking booking = getBookingById(id);
		if (null != booking)
			sessionFactory.getCurrentSession().delete(booking);
	}
	
	@SuppressWarnings("unchecked")
	public List<Booking> getBookingListByOwner(int id) {
		return sessionFactory.getCurrentSession().createQuery("from Booking where owner='"+id+"' order by dateIn desc").list();
	}

	/* Additional check for room to Book */
	public boolean checkRoomBeforeBook(SearchCriteria searchCriteria) {
		
				
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Booking.class);
		crit.add(Restrictions.eq("room.id", searchCriteria.getRoomId()));
		Disjunction or = Restrictions.disjunction();

		or.add(Restrictions.between("dateIn", searchCriteria.getDateIn(), searchCriteria.getDateOut()));
		or.add(Restrictions.between("dateOut", searchCriteria.getDateIn(), searchCriteria.getDateOut()));

		or.add(Restrictions.conjunction()
						.add(Restrictions.le("dateIn", searchCriteria.getDateIn()))
						.add(Restrictions.ge("dateOut", searchCriteria.getDateOut())));
		crit.add(or);
//		!
		crit.setMaxResults(1);
		if (crit.list().size()==0)
			return true;
		else {
			return false;
		}
		
	}

//	Util

	public String dateToString(Date date){
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(date);
	}
	
}
