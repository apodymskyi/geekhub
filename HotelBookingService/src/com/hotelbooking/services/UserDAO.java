package com.hotelbooking.services;

import java.util.List;

import com.hotelbooking.beans.User;

public interface UserDAO {
	public User getUserById(Integer id);
	public User getUserByEmail(String email);
	public List<User> getUsersList();
	public void saveUser(User user); 
	public void deleteUser(Integer id); 
	public boolean checkUser(String login);
	public User getLoggedUser();
}
