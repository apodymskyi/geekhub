package com.hotelbooking.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hotelbooking.beans.Photo;

//Author: Ihor Chanzhar

@Repository
@Transactional
public class PhotoDAOImpl implements PhotoDAO {

	@Autowired SessionFactory sessionFactory;

	public void savePhoto(Photo photo) {
		sessionFactory.getCurrentSession().saveOrUpdate(photo);
	}

	@SuppressWarnings("unchecked")
	public List<Integer> listByHotelId(Integer id) {
		return sessionFactory.getCurrentSession().createQuery("select id from Photo where hotel='"+id+"'").list();
				
	}

	@SuppressWarnings("unchecked")
	public List<Integer> listByRoomId(Integer id) {
		return sessionFactory.getCurrentSession().createQuery("select id from Photo where room='"+id+"'").list();
				
	}

	public Photo getPhotoById(Integer id){ 
		return (Photo) sessionFactory.getCurrentSession().get(Photo.class, id);
	}


	public void deletePhoto(Integer id) {
		Photo photo = getPhotoById(id);
		if (photo != null)
			sessionFactory.getCurrentSession().delete(photo);
	}


	public void deletePhotoByHotelId(Integer id) {
		sessionFactory.getCurrentSession().createQuery("delete from Photo where hotel='"+id+"'");
	}

	public void deletePhotoByRoomId(Integer id) {
		sessionFactory.getCurrentSession().createQuery("delete from Photo where room='"+id+"'");
		
	}
	
	public List<Photo> manageByHotelId(Integer id) {
		return sessionFactory.getCurrentSession().createQuery("from Photo where hotel='"+id+"'").list();
				
	}
	
	public List<Photo> manageByRoomId(Integer id) {
		return sessionFactory.getCurrentSession().createQuery("from Photo where room='"+id+"'").list();
		
	}
	
}
