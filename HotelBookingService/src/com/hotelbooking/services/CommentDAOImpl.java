package com.hotelbooking.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hotelbooking.beans.Comment;
import com.hotelbooking.beans.User;


@Repository
@Transactional
public class CommentDAOImpl implements CommentDAO {
	
	@Autowired SessionFactory sessionFactory;

	public void saveComment(Comment comment) {
		sessionFactory.getCurrentSession().saveOrUpdate(comment);
	}

	public void deleteComment(Comment comment) {
		sessionFactory.getCurrentSession().delete(comment);
	}
	
	public Comment getCommentById(Integer id){
		return (Comment) sessionFactory.getCurrentSession().get(Comment.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Comment> getCommentList() {
		return (List<Comment>) sessionFactory.getCurrentSession().createQuery("from Comment").list();
	}

	@SuppressWarnings("unchecked")
	public List<Comment> getHotelCommentList(Integer hotelID, Integer startResult, Integer showResults) {
		return (List<Comment>) sessionFactory.getCurrentSession().createQuery("from Comment where hotel.id="+hotelID+" order by post_date desc")
				.setFirstResult(startResult)
				.setMaxResults(showResults)
				.list();
	}

	@SuppressWarnings("unchecked")
	public List<Comment> getRoomCommentList(Integer roomID, Integer startResult, Integer showResults) {
		return (List<Comment>) sessionFactory.getCurrentSession().createQuery("from Comment where room.id="+roomID+" order by post_date desc")
				.setFirstResult(startResult)
				.setMaxResults(showResults)
				.list();
	}

	@SuppressWarnings("unchecked")
	public List<Comment> getUserCommentList(User userID) {
		return (List<Comment>) sessionFactory.getCurrentSession().createQuery("from Comment where user.id="+userID+" order by post_date desc").list();
	}

}
