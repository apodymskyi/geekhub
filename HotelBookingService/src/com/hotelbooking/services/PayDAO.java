package com.hotelbooking.services;

import com.hotelbooking.beans.Payment;

public interface PayDAO {
	public void save(Payment pay);
	public void deletePaymentById(Integer id);
	public Payment getPaymentById(Integer id);
	public Payment getPaymentByBookingId(Integer id);
}
