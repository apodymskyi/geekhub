package com.hotelbooking.services;

import java.util.List;

import com.hotelbooking.beans.Comment;
import com.hotelbooking.beans.User;

public interface CommentDAO {
	
	public void saveComment(Comment comment);
	public void deleteComment(Comment comment);
	public Comment getCommentById(Integer id);
	public List<Comment> getCommentList();
	public List<Comment> getHotelCommentList(Integer hotelID, Integer startResult, Integer showResults);
	public List<Comment> getRoomCommentList(Integer roomID, Integer startResult, Integer showResults);
	public List<Comment> getUserCommentList(User userID);
	
	

}
