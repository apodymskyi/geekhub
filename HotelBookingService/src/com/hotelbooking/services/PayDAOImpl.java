package com.hotelbooking.services;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hotelbooking.beans.Payment;

@Repository
@Transactional
public class PayDAOImpl implements PayDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	public void save(Payment pay) {
		sessionFactory.getCurrentSession().saveOrUpdate(pay);		
	}

	public Payment getPaymentByBookingId(Integer id) {
		return (Payment) sessionFactory.getCurrentSession().createQuery("from Payment where booking='"+id+"'");
	}

	public Payment getPaymentById(Integer id) {
		return (Payment) sessionFactory.getCurrentSession().createQuery("from Payment where id='"+id+"'");
	}

	public void deletePaymentById(Integer id) {
		Payment payment = getPaymentById(id);
		if (null!=payment)
			sessionFactory.getCurrentSession().delete(payment);
	}

}
