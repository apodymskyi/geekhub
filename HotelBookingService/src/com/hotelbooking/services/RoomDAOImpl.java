package com.hotelbooking.services;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hotelbooking.beans.Room;
import com.hotelbooking.beans.SearchCriteria;

@Repository
@Transactional
public class RoomDAOImpl implements RoomDAO {

	@Autowired SessionFactory sessionFactory;
	
	public void saveRoom(Room room) {
		sessionFactory.getCurrentSession().saveOrUpdate(room);		
	}

	public void deleteRoom(Integer id) {
		Room room = getRoomById(id);
		if(room!=null)
			sessionFactory.getCurrentSession().delete(room);
		
	}

	@SuppressWarnings("unchecked")
	public List<Room> getRoomListByCriteria(SearchCriteria criteria) {
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Room.class);
		if (criteria.getCountry()!=null)
			crit.add(Restrictions.eq("country", (criteria.getCountry())));
		if (criteria.getCity()!=null)
			crit.add(Restrictions.eq("city", (criteria.getCity())));
//		if (criteria.getDateIn()!=null)&&
//			crit.add(Restrictions.eq("city", (criteria.getCity())));
		crit.addOrder(Order.asc("price"));			
		return (List<Room>) crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Room> getRoomListByHotel(Integer hotelId) {
		return  sessionFactory.getCurrentSession().createQuery("from Room where hotel="+hotelId).list();
		
	}

	public Room getRoomById(Integer roomID) {
		return (Room) sessionFactory.getCurrentSession().get(Room.class, roomID);
	}

	@SuppressWarnings("unchecked")
	public List<Room> getRoomList() {
		return  sessionFactory.getCurrentSession().createQuery("from Room").list();
	}


	@SuppressWarnings("unchecked")
	public void deleteRoomByHotelId(Integer id) {
		List<Room> rooms = sessionFactory.getCurrentSession().createQuery("from Room where hotel='"+id+"'").list();
		for (Room room : rooms){
			sessionFactory.getCurrentSession().delete(room);
		}
		
	}

}