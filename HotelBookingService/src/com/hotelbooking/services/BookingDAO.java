package com.hotelbooking.services;

import java.util.List;

import com.hotelbooking.beans.Booking;
import com.hotelbooking.beans.Room;
import com.hotelbooking.beans.SearchCriteria;

public interface BookingDAO {
	
	public void saveBooking(Booking booking);
	public void deleteBooking(Integer id);
	public Booking getBookingById(Integer id);
	public List<Booking> getBookingList();
	public List<Booking> getBookingListByHotel(int id, int show);
	public List<Booking> getBookingListByOwner(int id);
	public List<Booking> getBookingByCriteria(SearchCriteria searchCriteria);
	public List<Room> getRoomsByCriteria(SearchCriteria searchCriteria);
	public boolean checkRoomBeforeBook(SearchCriteria searchCriteria);

}
