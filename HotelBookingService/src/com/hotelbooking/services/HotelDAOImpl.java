package com.hotelbooking.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hotelbooking.beans.Hotel;
import com.hotelbooking.beans.SearchCriteria;

import util.StringUtil;

@Repository
@Transactional
public class HotelDAOImpl implements HotelDAO {

	@Autowired
	SessionFactory sessionFactory;

	public void saveHotel(Hotel hotel) {
		sessionFactory.getCurrentSession().saveOrUpdate(hotel);
	}

	public void deleteHotel(Integer id) {
		Hotel hotel = getHotelById(id);
		if (hotel != null)
			sessionFactory.getCurrentSession().delete(hotel);

	}

	public Hotel getHotelById(Integer id) {
		return (Hotel) sessionFactory.getCurrentSession().get(Hotel.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Hotel> getHotelList() {
		return sessionFactory.getCurrentSession().createQuery("from Hotel")
				.list();
	}

	@SuppressWarnings("unchecked")
	public List<Hotel> getMyHotelList(Integer id) {
		return sessionFactory.getCurrentSession().createQuery("from Hotel where owner='"+id+"'").list();
				
	}

	@SuppressWarnings("unchecked")
	public List<String> getCountries() {
		return sessionFactory
				.getCurrentSession()
				.createCriteria(Hotel.class)
				.setProjection(
						Projections.distinct(Property.forName("country"))).list();
	}

	@SuppressWarnings("unchecked")
	public List<String> getCities() {
		return sessionFactory.getCurrentSession().createCriteria(Hotel.class)
				.setProjection(Projections.distinct(Property.forName("city"))).list();
	}

	@SuppressWarnings("unchecked")
	public List<String> getCitiesByCountry(String country) {
		return (List<String>) sessionFactory
				.getCurrentSession()
				.createCriteria(Hotel.class)
				.add(Restrictions.eq("country", country))
				.setProjection(
						Projections.distinct(Property.forName("city"))).list();
	}

	@SuppressWarnings("unchecked")
	public List<Hotel> getHotelListByLocation(SearchCriteria criteria) {
		
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Hotel.class);
		
		if (StringUtil.strNotNullOrEmpty(criteria.getCountry()))
			crit.add(Restrictions.eq("country", criteria.getCountry()));
		if (StringUtil.strNotNullOrEmpty(criteria.getCity()))
			crit.add(Restrictions.eq("city", criteria.getCity()));
		return (List<Hotel>) crit.list();
	}

}
