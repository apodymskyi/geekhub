package com.hotelbooking.services;

import java.util.List;

import com.hotelbooking.beans.Hotel;
import com.hotelbooking.beans.SearchCriteria;

public interface HotelDAO {
	public void saveHotel(Hotel hotel);
	public void deleteHotel(Integer id);
	public Hotel getHotelById(Integer id);
	public List <String> getCountries();
    public List <String> getCities();
    public List <String> getCitiesByCountry(String country);
    public List<Hotel> getHotelListByLocation(SearchCriteria criteria);
    public List<Hotel> getHotelList();
    public List<Hotel> getMyHotelList(Integer id);
}
