package com.hotelbooking.controllers;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hotelbooking.beans.User;
import com.hotelbooking.services.UserDAO;

//Author: Ihor Chanzhar

@Controller
public class UserController {
	
	@Autowired UserDAO userDAO;
	
	/*list of all users
	 * Access only for users with authority 'ROLE_ADMIN'
	 * */
	@RequestMapping(value="listUsers.action")
	public String list(ModelMap map) {
		map.put("users",userDAO.getUsersList());
		return "listUsers";
	}
	/*Load User Entity by ID for editing if LoggedUser is this User 
	 * if ID null or invalid instantiate new User
	 */
	@RequestMapping(value="loadUser.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map) {
		if((id!=null) && (userDAO.getLoggedUser().getId().equals(id))) {
			User user = userDAO.getUserById(id);
			map.put("user", user);
		} else {
			map.put("user", new User());
		}
		return "register";
	}
	/*Load User Entity by ID for editing if LoggedUser has authority ROLE_ADMIN, 
	 * if ID null instantiate new User 
	 */
	@RequestMapping(value="loadUser.action")
	public String loadAction(@RequestParam(value="id", required=true) Integer id, ModelMap map) {
		User user = id == null ? new User() : userDAO.getUserById(id);
		map.put("user", user);
		return "editUser";
	}
	//Delete User only if LoggedUser has authority 'ROLE_ADMIN'
	@RequestMapping(value="deleteUser.action")
	public String delete(@RequestParam(value="id", required=true) Integer id) {
		userDAO.deleteUser(id);
		return "listUsers";
	}
	//Save new User  
	@RequestMapping(value="saveUser.html")
	public String save(User user) {
		String password = user.getPassword();
		user.setPassword(DigestUtils.md5Hex(password));
		userDAO.saveUser(user);
		return "congratulations";
	}
	
	//Save User - only for editing by admin
	@RequestMapping(value="saveUser.action")
	public String saveUser(User user) {
		userDAO.saveUser(user);
		return "congratulations";
	}
	
	//Checking Email registered or not
	@RequestMapping(value = "check.html")
	  public @ResponseBody String checkEmail(@RequestParam(value="email", required=true) String email) {
		String result = "OK";	
		if(userDAO.checkUser(email))
			result = "Not Unique";
		return result;
	  }
	
	
	//Getting logged user
	@RequestMapping(value = "getLogged.html")
	public void getLogged(ModelMap map){
		User user = userDAO.getLoggedUser();
		if(user!=null)
		map.put("loggedUser", user);
		
	}
}
