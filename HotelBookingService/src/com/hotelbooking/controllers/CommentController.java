package com.hotelbooking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hotelbooking.beans.Comment;
import com.hotelbooking.beans.User;
import com.hotelbooking.services.BookingDAO;
import com.hotelbooking.services.CommentDAO;
import com.hotelbooking.services.HotelDAO;
import com.hotelbooking.services.RoomDAO;
import com.hotelbooking.services.UserDAO;

import util.StringUtil;

@Controller
public class CommentController {

	@Autowired
	HotelDAO hotelDAO;
	@Autowired
	RoomDAO roomDAO;
	@Autowired
	UserDAO userDAO;
	@Autowired
	BookingDAO bookingDAO;
	@Autowired
	CommentDAO commentDAO;

	@RequestMapping(value = "addRoomComment.html")
	public String addRoomComment(
			@RequestParam(value = "room.id", required = true) Integer roomID,
			@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "startResult", required = false) Integer startResult,
			@RequestParam(value = "showResults", required = false) Integer showResults,
			ModelMap map) {
		Integer start = null == startResult ? 0 : startResult;
		Integer show = null == showResults ? 10 : showResults;
		User user = userDAO.getLoggedUser();
		map.put("loggedUserID", user.getId());

		if (makeComment(user, null, roomID, text))
			map.put("commentStatus", "Comment added=)");
		else
			map.put("commentStatus", "Comment declined =(");

		map.put("comments", commentDAO.getRoomCommentList(roomID, start, show));
		return "comments";
	}

	@RequestMapping(value = "addHotelComment.html")
	public String addHotelComment(
			@RequestParam(value = "hotel.id", required = true) Integer hotelID,
			@RequestParam(value = "text", required = false) String text,
			@RequestParam(value = "startResult", required = false) Integer startResult,
			@RequestParam(value = "showResults", required = false) Integer showResults,
			ModelMap map) {
		Integer start = null == startResult ? 0 : startResult;
		Integer show = null == showResults ? 10 : showResults;
		User user = userDAO.getLoggedUser();
		map.put("loggedUserID", user.getId());
		if (makeComment(user, hotelID, null, text))
			map.put("commentStatus", "Comment added=)");
		else
			map.put("commentStatus", "Comment declined =(");

		map.put("comments",
				commentDAO.getHotelCommentList(hotelID, start, show));
		return "comments";
	}

	@RequestMapping(value = "deleteComment.html")
	public String deleteComment(
			@RequestParam(value = "comment.id", required = true) Integer commentID,
			ModelMap map) {
		User user = userDAO.getLoggedUser();
		Comment comment = commentDAO.getCommentById(commentID);
		if (user.getId().equals(comment.getOwner().getId())) {
			commentDAO.deleteComment(comment);
			map.put("status", "Your comment vanished");
		} else
			// Paranoid check
			map.put("status", "You cannot delete this comment");
		return "result";
	}

	@RequestMapping(value = "deleteCommentByMgr.html")
	public String deleteCommentByMgr(
			@RequestParam(value = "comment.id", required = true) Integer commentID,
			ModelMap map) {
		User user = userDAO.getLoggedUser();
		Comment comment = commentDAO.getCommentById(commentID);
		if (canDeleteComment(user, comment)) {
			commentDAO.deleteComment(comment);
			map.put("status", "Comment vanished");
		} else
			// Paranoid check
			map.put("status", "You cannot delete this comment");
		return "result";
	}

	@RequestMapping(value = "listRoomComments.html")
	public String listRoomComments(
			@RequestParam(value = "room.id", required = true) Integer roomID,
			@RequestParam(value = "startResult", required = false) Integer startResult,
			@RequestParam(value = "showResults", required = false) Integer showResults,
			ModelMap map) {
		Integer start = null == startResult ? 0 : startResult;
		Integer show = null == showResults ? 10 : showResults;
		map.put("comments", commentDAO.getRoomCommentList(roomID, start, show));
		User user = userDAO.getLoggedUser();
		if (null!=user)
			map.put("loggedUserID", user.getId());
		return "comments";
	}

	@RequestMapping(value = "listHotelComments.html")
	public String listHotelComments(
			@RequestParam(value = "hotel.id", required = true) Integer hotelID,
			@RequestParam(value = "startResult", required = false) Integer startResult,
			@RequestParam(value = "showResults", required = false) Integer showResults,
			ModelMap map) {
		Integer start = null == startResult ? 0 : startResult;
		Integer show = null == showResults ? 10 : showResults;
		map.put("comments", commentDAO.getHotelCommentList(hotelID, start, show));
		User user = userDAO.getLoggedUser();
		if (null!=user)
			map.put("loggedUserID", user.getId());
		return "comments";
	}

	// Util

	public boolean makeComment(User user, Integer hotelID, Integer roomID,
			String text) {
		if ((user != null) && (StringUtil.strNotNullOrEmpty(text))) {
			Comment comment = new Comment();
			comment.setOwner(user);

			if (roomID != null)
				comment.setRoom(roomDAO.getRoomById(roomID));
			else if (hotelID != null)
				comment.setHotel(hotelDAO.getHotelById(hotelID));
			else
				return false;

			comment.setText(text);
			commentDAO.saveComment(comment);
			return true;
		} else
			return false;
	}

	public boolean canDeleteComment(User user, Comment comment) {
		if ("ROLE_ADMIN".equals(user.getRole())) {
			return true;
		}
		else if (null != comment.getHotel()) {
			if (user.getId().equals(comment.getHotel().getOwner().getId()))
				return true;
			else
				return false;
		} else if (null != comment.getRoom()) {
			if (user.getId().equals(
					comment.getRoom().getHotel().getOwner().getId()))
				return true;
			else
				return false;
		}
		else
			return false;
	}

}
