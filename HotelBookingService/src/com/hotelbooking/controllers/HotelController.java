package com.hotelbooking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hotelbooking.beans.Hotel;
import com.hotelbooking.beans.SearchCriteria;
import com.hotelbooking.beans.User;
import com.hotelbooking.services.BookingDAO;
import com.hotelbooking.services.HotelDAO;
import com.hotelbooking.services.PhotoDAO;
import com.hotelbooking.services.RoomDAO;
import com.hotelbooking.services.UserDAO;

@Controller
public class HotelController {

	@Autowired
	HotelDAO hotelDAO;
	@Autowired
	UserDAO userDAO;
	@Autowired
	PhotoDAO photoDAO;
	@Autowired
	BookingDAO bookingDAO;
	@Autowired
	RoomDAO roomDAO;

	// List All hotel from DataBase

	@RequestMapping(value = "find-a-hotel.html")
	public String listhotels() {
		return "find-a-hotel";
	}

	@RequestMapping(value = "hotels.html")
	public String list(
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "country", required = false) String country,
			ModelMap map) {

		SearchCriteria criteria = new SearchCriteria();
		criteria.setCountry(country);
		criteria.setCity(city);
		map.put("hotels", hotelDAO.getHotelListByLocation(criteria));
		return "hotels";
	}

	// List Hotels where LogedUser is Owner
	@RequestMapping(value = "listMyHotels.html")
	public String myList(ModelMap map) {
		User user = userDAO.getLoggedUser();
		if (user != null) {
			map.put("hotels", hotelDAO.getMyHotelList(user.getId()));
		}
		return "myhotels";
	}

	// Save entity Hotel to DataBase
	@RequestMapping(value = "saveHotel.html")
	public String save(Hotel hotel, ModelMap map) {
		User user = userDAO.getLoggedUser();
		if (user != null) {
			if ("ROLE_HOTEL".equals(user.getRole()) || "ROLE_ADMIN".equals(user.getRole()))
				hotelDAO.saveHotel(hotel);
			else
				return "accessdenied";
		}
		return "successful";
	}

	/*
	 * Delete Hotel and all dependencies. Only if user owner of this hotel or
	 * has authority 'ROLE_ADMIN' If user not owner or hadn't 'ROLE_ADMIN'
	 * redirect AccessDenied page
	 */
	@RequestMapping(value = "deleteHotel.html")
	public String delete(
			@RequestParam(value = "id", required = true) Integer id,
			ModelMap map) {
		Hotel hotel = hotelDAO.getHotelById(id);
		if (hotel != null) {
			User user = userDAO.getLoggedUser();
			if ((hotel.isOwner(user) && "ROLE_HOTEL".equals(user.getRole())) || "ROLE_ADMIN".equals(user.getRole())) {
				photoDAO.deletePhotoByHotelId(id);
				hotelDAO.deleteHotel(id);
			} else
				return "accessdenied";
		}
		map.put("hotels", hotelDAO.getHotelList());
		return "hotels";
	}

	/*
	 * Load Hotel Entity by ID for editing if LoggedUser is owner of this Hotel
	 * or has authority ROLE_ADMIN, if ID null or invalid instantiate new Hotel
	 * If user not logged redirect AccessDenied page
	 */
	@RequestMapping(value = "loadHotel.html")
	public String load(
			@RequestParam(value = "id", required = false) Integer id,
			ModelMap map) {
		User user = userDAO.getLoggedUser();
		if (id != null) {
			Hotel hotel = hotelDAO.getHotelById(id);
			if ((hotel != null)	&& ((hotel.isOwner(user) 
					&& "ROLE_HOTEL".equals(user.getRole())) || "ROLE_ADMIN".equals(user.getRole()))) {
				map.put("hotel", hotel);
				return "editHotel";
			} else
				return "accessdenied";
		} else
			map.put("hotel", new Hotel());
		map.put("loggedUser", user);
		return "hotel";
	}

	// Show detailed hotel information
	@RequestMapping(value = "hotelInfo.html")
	public String info(@RequestParam(value = "id") Integer id, ModelMap map) {
		map.put("hotel", hotelDAO.getHotelById(id));
		map.put("photos", photoDAO.listByHotelId(id));
		return "hotelInfo";
	}

	@RequestMapping(value = "countries.html")
	public String countries(ModelMap map) {
		map.put("countries", hotelDAO.getCountries());
		return "country";
	}

	@RequestMapping(value = "cities.html")
	public String cities(@RequestParam(value = "country") String country,
			ModelMap map) {
		map.put("cities", hotelDAO.getCitiesByCountry(country));
		return "city";
	}

	public boolean strNotNullOrEmpty(String str) {
		if (str == (null) || str.equals(""))
			return false;
		else
			return true;
	}
}
