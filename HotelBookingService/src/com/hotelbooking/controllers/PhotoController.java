package com.hotelbooking.controllers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hotelbooking.beans.Hotel;
import com.hotelbooking.beans.Photo;
import com.hotelbooking.beans.Room;
import com.hotelbooking.beans.User;
import com.hotelbooking.services.HotelDAO;
import com.hotelbooking.services.PhotoDAO;
import com.hotelbooking.services.RoomDAO;
import com.hotelbooking.services.UserDAO;

//Author: Ihor Chanzhar

@Controller
public class PhotoController {

	@Autowired PhotoDAO photoDAO;
	@Autowired HotelDAO hotelDAO;
	@Autowired RoomDAO roomDAO;
	@Autowired UserDAO userDAO;

	// Uploading Photo of hotel to DataBase

	@RequestMapping(value = "uploadImage.html", method = RequestMethod.POST)
	public String upload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "scope", required = true) String scope,
			ModelMap map) throws IOException {
		if (file != null && file.getContentType().startsWith("image/")){
			if ("hotel".equals(scope) || "room".equals(scope)) {
				Photo photo = new Photo();
	
				if ("hotel".equals(scope)) {
					Hotel hotel = hotelDAO.getHotelById(id);
					photo.setHotel(hotel);
				} else {
					Room room = roomDAO.getRoomById(id);
					photo.setRoom(room);
				}
	
				photo.setFilename(file.getOriginalFilename());
				photo.setPhoto(file.getBytes());
				photo.setSize(file.getSize());
				photo.setContentType(file.getContentType());
				photoDAO.savePhoto(photo);
				
				map.put("status", "Photo "+ photo.getFilename() + " uploaded");
			} else
				map.put("status", "Uploading failed...");
		} else
			map.put("status", "Uploading failed... Wrong file format");
		return "result";

	}

	// Show Hotel Photo in view
	@RequestMapping(value = "getPhoto.html")
	public void show(ModelMap map, HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "id", required = true) Integer id) {
		Photo photo = photoDAO.getPhotoById(id);
		if (null != photo) {
			response.setContentType(photo.getContentType());

			byte[] img = photo.getPhoto();
			try {
				BufferedInputStream input = new BufferedInputStream(
						new ByteArrayInputStream(img));
				BufferedOutputStream output = new BufferedOutputStream(
						response.getOutputStream());
				byte[] buffer = new byte[4096];
				int length;
				while ((length = input.read(buffer)) > 0) {
					output.write(buffer, 0, length);
				}
				output.close();
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@RequestMapping(value = "deletePhoto.html")
	public void delete(@RequestParam(value = "id", required = true) Integer id) {
//		Paranoid check
		User user = userDAO.getLoggedUser();
		if ((user!=null)&&(("ROLE_HOTEL".equals(user.getRole())||"ROLE_ADMIN".equals(user.getRole()))))
			photoDAO.deletePhoto(id);
	}

	@RequestMapping(value = "managePhoto.html")
	public String managePhoto(
			@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "scope", required = true) String scope,
			ModelMap map) {
		
//		Paranoid check
		User user = userDAO.getLoggedUser();
		if (user==null)
			return "accessdenied";
		if (!("ROLE_HOTEL".equals(user.getRole())||"ROLE_ADMIN".equals(user.getRole())))
			return "accessdenied";
		
		if ("hotel".equals(scope))
			map.put("photos", photoDAO.manageByHotelId(id));
		if ("room".equals(scope))
			map.put("photos", photoDAO.manageByRoomId(id));
		return "managePhoto";
	}

}
