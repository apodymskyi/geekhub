package com.hotelbooking.controllers;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.hotelbooking.beans.Booking;
import com.hotelbooking.beans.Payment;
import com.hotelbooking.beans.User;
import com.hotelbooking.services.BookingDAO;
import com.hotelbooking.services.PayDAO;
import com.hotelbooking.services.UserDAO;

@Controller
public class MerchantGateway {
	BraintreeGateway gateway = new BraintreeGateway(
			  Environment.SANDBOX,
			  "232bz8pnjcj3z2zb",
			  "n69yqz7kbqcjmds6",
			  "04a72435ba3f1b717a4049f6b257e9be"
			);
	
	@Autowired PayDAO payDAO;
	@Autowired BookingDAO bookingDAO;
	@Autowired UserDAO userDAO;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, true));

	}
	
	@RequestMapping(value="/pay.html")
	public String pay(Booking booking, ModelMap map){
			bookingDAO.saveBooking(booking);
			map.put("bookingId", booking.getId());
		return "pay";
	}
	
	@RequestMapping(value="/payAnonymous.html")
	public String payAnonimous(Booking booking, User user, ModelMap map){
			
		userDAO.saveUser(user);
		User savedUser = userDAO.getUserByEmail(user.getEmail());
		booking.setOwner(savedUser);
		bookingDAO.saveBooking(booking);
		map.put("bookingId", booking.getId());
			
		return "pay";
	}
	
	@RequestMapping(value="/create_transaction.html")
	public String handle(
			@RequestParam(value="number", required=true) String number,
			@RequestParam(value="cvv", required=true) String cvv,
			@RequestParam(value="date", required=true) String date,
			@RequestParam(value="id", required=true) Integer id, ModelMap map) {

		//total cost of room
		Booking booking = bookingDAO.getBookingById(id);
		DateTime dateIN = new DateTime(booking.getDateIn());
		DateTime dateOUT = new DateTime(booking.getDateOut());
		int days =Days.daysBetween(dateIN, dateOUT).getDays();
		Integer cost = days*booking.getRoom().getPrice();
		
		TransactionRequest transactionRequest = new TransactionRequest()
            .amount(new BigDecimal(cost))
            .creditCard()
                .number(number)
                .cvv(cvv)
                .expirationDate(date)
                .done()
            .options()
                .submitForSettlement(true)
                .done();

        Result<Transaction> result = gateway.transaction().sale(transactionRequest);

        
        if (result.isSuccess()) {
        	String res = result.getTarget().getId();
        	map.put("result", res);

        	Payment payment = new Payment();
        	payment.setName(res);
        	payment.setCost(cost);
        	payDAO.save(payment);
  
        	booking.setPayment(payment);
        	bookingDAO.saveBooking(booking);
        	
          return "successPay";
        } else {
        	bookingDAO.deleteBooking(id);
        	map.put("result", result.getMessage());
          return "errorPay";
        }
    }
}
