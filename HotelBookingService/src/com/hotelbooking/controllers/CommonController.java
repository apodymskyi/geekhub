package com.hotelbooking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hotelbooking.beans.User;
import com.hotelbooking.services.BookingDAO;
import com.hotelbooking.services.HotelDAO;
import com.hotelbooking.services.RoomDAO;
import com.hotelbooking.services.UserDAO;

@Controller
public class CommonController {

	@Autowired UserDAO userDAO;
	@Autowired RoomDAO roomDAO;
	@Autowired HotelDAO hotelDAO;
	@Autowired BookingDAO bookingDAO;

	@RequestMapping(value = "/")
	public String index(ModelMap map) {
		User user = userDAO.getLoggedUser();
			if (null != user)
				map.put("loggedUser", user);
		return "index";
	}

	@RequestMapping(value = "register.html")
	public String register() {
		return "register";
	}
	
	@RequestMapping(value="/loginfailed.html")
	public String loginerror(ModelMap model) {
 		model.addAttribute("error", "Wrong E-mail or Password");
		return "index";
	}
	
}
