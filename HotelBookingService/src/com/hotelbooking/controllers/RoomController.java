package com.hotelbooking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hotelbooking.beans.Room;
import com.hotelbooking.beans.User;
import com.hotelbooking.services.CommentDAO;
import com.hotelbooking.services.HotelDAO;
import com.hotelbooking.services.PhotoDAO;
import com.hotelbooking.services.RoomDAO;
import com.hotelbooking.services.UserDAO;

@Controller
public class RoomController {
	
	@Autowired UserDAO userDAO;
	@Autowired RoomDAO  roomDAO;
	@Autowired HotelDAO hotelDAO;
	@Autowired CommentDAO commentDAO;
	@Autowired PhotoDAO photoDAO;


//	@RequestMapping(value="search.html")
//	public String search(@RequestParam(value="country", required=false) String country,
//						 @RequestParam(value="city", required=false) String city,
//						 @RequestParam(value="type", required=false) RoomType type,
//						 ModelMap map){
//
//		map.put("countries", hotelDAO.getCountries());
//		map.put("cities", hotelDAO.getCities());
//		return "search";
//	}
	
	//saving Room
	@RequestMapping(value="saveRoom.html")
	public String save(Room room, ModelMap map){
		roomDAO.saveRoom(room);
	return "successful";
	}
	
	//redirect to add Room for Hotel by Hotel_ID
	@RequestMapping(value="addRoom.html")
	public String addRoom(@RequestParam(value="id", required=true) Integer id, ModelMap map){
		map.put("hotelId",id);
		return "room";
	}
	
	//Getting list of rooms by hotel_id
	@RequestMapping(value="roomsByHotel.html")
	public String roomsByHotel(@RequestParam(value="id", required=true) Integer id, ModelMap map){
		map.put("rooms", roomDAO.getRoomListByHotel(id));
		map.put("hotel", hotelDAO.getHotelById(id));
		
		return "rooms";
	}
	
	@RequestMapping(value="myrooms.html")
	public String myroomsByHotel(@RequestParam(value="id", required=true) Integer id, ModelMap map){
		map.put("hotel", hotelDAO.getHotelById(id).getName());
		map.put("rooms", roomDAO.getRoomListByHotel(id));
			
		return "myrooms";
	}
	
	/*Load room by id for editing if loggedUser owner of hotel which is associated with this Room
	 * or user has authority 'ROLE_ADMIN'
	 * If user not logged redirect to AccesDenied page
	 */
	@RequestMapping(value="loadRoom.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map){
		User user = userDAO.getLoggedUser();
		if(user!=null){
			Room room = roomDAO.getRoomById(id);
			if ((room!=null) && ("ROLE_ADMIN".equals(user.getRole()) || room.getHotel().isOwner(user))){
				map.put("hotelId", room.getHotel().getId());
				map.put("room", room);
			} else
				map.put("room", new Room());
		} else
			return "accessdenied";
		
		return "room";
	}
	
	/*delete Room if loggedUser owner of hotel which is associated with this Room
	 * or user has authority 'ROLE_ADMIN'
	 */
	@RequestMapping(value="deleteRoom.html")
	public void delete(@RequestParam(value="id", required=true) Integer id, ModelMap map){
		User user = userDAO.getLoggedUser();
		if(user!=null){
			Room room = roomDAO.getRoomById(id);
			if ((room!=null) && ("ROLE_ADMIN".equals(user.getRole()) || room.getHotel().isOwner(user))){
				roomDAO.deleteRoom(id);
			}
		}
	}
	
	
	@RequestMapping(value = "roomInfo.html")
	public String info(@RequestParam(value = "id") Integer id, ModelMap map) {
		map.put("room", roomDAO.getRoomById(id));
		map.put("photos", photoDAO.listByRoomId(id));
		return "roomInfo";
	}
}

