package com.hotelbooking.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import util.StringUtil;

import com.hotelbooking.beans.Booking;
import com.hotelbooking.beans.BookingStatus;
import com.hotelbooking.beans.Room;
import com.hotelbooking.beans.RoomType;
import com.hotelbooking.beans.SearchCriteria;
import com.hotelbooking.beans.User;
import com.hotelbooking.services.BookingDAO;
import com.hotelbooking.services.CommentDAO;
import com.hotelbooking.services.HotelDAO;
import com.hotelbooking.services.PhotoDAO;
import com.hotelbooking.services.RoomDAO;
import com.hotelbooking.services.UserDAO;

@Controller
public class BookingController {

	@Autowired HotelDAO hotelDAO;
	@Autowired RoomDAO roomDAO;
	@Autowired UserDAO userDAO;
	@Autowired BookingDAO bookingDAO;
	@Autowired CommentDAO commentDAO;
	@Autowired PhotoDAO photoDAO;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, true));

	}

	/*
	 * Load Booking Entity by ID for editing if LoggedUser is owner of this
	 * Booking or owner of Hotel which is associated with this Booking or has
	 * authority ROLE_ADMIN, if ID null or invalid instantiate new Hotel If user
	 * not logged redirect AccessDenied page
	 */
	@RequestMapping(value = "loadBooking.html")
	public String load(
			@RequestParam(value = "id", required = false) Integer id,
			ModelMap map) {

		if (userIsAdminOrOwner(id)) {
			Booking booking = bookingDAO.getBookingById(id);
			map.put("booking", booking);

			map.put("rooms", roomDAO.getRoomListByHotel(booking.getHotel().getId()));
			return "editBooking";
		} else
			return "accessdenied";
	}

	/*
	 * Delete Booking entity if logged user is Hotel/Booking owner or has
	 * authority ROLE_ADMIN
	 */
	@RequestMapping(value = "deleteBooking.html")
	public String delete(
			@RequestParam(value = "id", required = true) Integer bookID,
			ModelMap map) {

		if (userIsAdminOrOwner(bookID)) {
			bookingDAO.deleteBooking(bookID);
			String status = "Booking with id: " + bookID + " deleted";
			map.put("status", status);
			return "successful";
		} else
			return "accessdenied";
	}

	/* Save booking with custom parsing DateIn DateOut via string */
	@RequestMapping(value = "saveBooking.html")
	public String save(Booking booking, ModelMap map) {
		
		String status = "";

		if ((booking.getDateIn() == null) || (booking.getDateIn() == null)) {
			status = "Wrong Date In and/or Date Out ";
			map.put("status", status);
			return "result";
		}
		
		if (checkRoomToBeFree(booking.getRoom().getId(),booking.getDateIn(),booking.getDateOut())) {
			bookingDAO.saveBooking(booking);
			status = "Booking saved! Your booking id: " + booking.getId()
					+ " . See list of your bookings in main menu";
		} else {
			status = "Something went wrong. Possible reasons: time-out or room is not available";
		}
		map.put("status", status);
		return "result";
	}
	
	@RequestMapping(value = "saveEditedBooking.html")
	public String saveEditedBooking(Booking booking, ModelMap map) {
			bookingDAO.saveBooking(booking);
		return "successful";
	}
	
	@RequestMapping(value = "saveBookingAnonymous.html")
	public String saveAnonymous(Booking booking, User user, ModelMap map) {
		userDAO.saveUser(user);
		User savedUser = userDAO.getUserByEmail(user.getEmail());
		booking.setOwner(savedUser);
		bookingDAO.saveBooking(booking);
		String status = "Booking accepted.";
		map.put("status", status);
		return "successful";
	}

	/* List free rooms by filtering parameters */
	
	@RequestMapping(value = "find-a-room.html")
	public String showFreeRoomsForm(ModelMap map){
		map.put("cities",hotelDAO.getCities());
		return "find-a-room";
	}
	@RequestMapping(value = "listFreeRooms.html")
	public String showFreeRooms(
			@RequestParam(value = "occupancy", required = false) Integer occupancy,
			@RequestParam(value = "type", required = false) String type,
			@RequestParam(value = "price", required = false) Integer price,
			@RequestParam(value = "hotelId", required = false) Integer hotelId,
			@RequestParam(value = "country", required = false) String country,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "dateIn", required = false) String dateIn,
			@RequestParam(value = "dateOut", required = false) String dateOut,
			@RequestParam(value = "status", required = false) String bookingStatus,
			@RequestParam(value = "limit", required = false) Integer limit,

			ModelMap map) {

		SearchCriteria criteria = new SearchCriteria();

		int limitReal = limit == null ? 10 : limit;
		criteria.setLimitResults(limitReal);

		// ROOM filter by Occupancy,Type,price
		criteria.setRoomOccupancy(occupancy);
		criteria.setRoomPrice(price);
	
		if (StringUtil.strNotNullOrEmpty(type)) {
			RoomType roomT = RoomType.valueOf(type);
			criteria.setRoomType(roomT);
		}

		// ROOM filter by Hotel, Country, City
//		criteria.setHotelId(hotelId);
		criteria.setCountry(country);
		criteria.setCity(city);

		// ROOM filter by Books' (Dates and Status)

		if (StringUtil.strNotNullOrEmpty(bookingStatus)) {
			BookingStatus bookingS = BookingStatus.valueOf(bookingStatus);
			criteria.setBookingStatus(bookingS);
		}
		if (StringUtil.strNotNullOrEmpty(dateIn) && StringUtil.strNotNullOrEmpty(dateOut)) {

			Date dateInDate = StringUtil.stringToDate(dateIn);
			criteria.setDateIn(dateInDate);
			Date dateOutDate = StringUtil.stringToDate(dateOut);
			criteria.setDateOut(dateOutDate);
			map.put("dateIn", dateIn);
			map.put("dateOut", dateOut);
		}
		
//		criteria.setBookingStatus(BookingStatus.valueOf(bookingStatus));
		
		List<Room> foundRooms = bookingDAO.getRoomsByCriteria(criteria);
		
		map.put("rooms", foundRooms);
		map.put("roomsFound", foundRooms.size());

		return "listFreeRooms";
	}

	@RequestMapping(value = "addBooking.html")
	public String addBooking(
			@RequestParam(value = "hotel_id", required = true) Integer hotelId,
			@RequestParam(value = "room_id", required = true) Integer roomId,
			@RequestParam(value = "dateIn", required = false) String dateIn,
			@RequestParam(value = "dateOut", required = false) String dateOut,
			ModelMap map) {

		Booking booking = new Booking();

		if (StringUtil.strNotNullOrEmpty(dateIn) && StringUtil.strNotNullOrEmpty(dateOut)) {
			booking.setDateIn(StringUtil.stringToDate(dateIn));
			booking.setDateOut(StringUtil.stringToDate(dateOut));
		}

		map.put("booking", booking);
		map.put("hotel", hotelDAO.getHotelById(hotelId));
		map.put("room", roomDAO.getRoomById(roomId));
		map.put("photos", photoDAO.listByRoomId(roomId));
		
		User user = userDAO.getLoggedUser();
		if (user != null) {
			map.put("loggedUser", user);
			return "booking";
		} else
			return "bookingAnonymous";
	}

	@RequestMapping(value = "manageBookings.html")
	public String hotelBooking(
			@RequestParam(value = "id", required = false) Integer id,
			@RequestParam(value = "show", required = false) Integer show,
			ModelMap map) {
		if (id == null) {
			User user = userDAO.getLoggedUser();
			if (user != null) {
				map.put("hotels", hotelDAO.getMyHotelList(user.getId()));
			}
		} else {
			int showResults = show==null? 50 : show;
			map.put("status", hotelDAO.getHotelById(id).getName());
			map.put("hotelID", id);
			map.put("showResults", showResults);
			map.put("bookings", bookingDAO.getBookingListByHotel(id,showResults));
		}
		return "manageBookings";
	}

	@RequestMapping(value = "myBookings.html")
	public String mybookings(ModelMap map) {

		User user = userDAO.getLoggedUser();
		if (user != null) {
			map.put("bookings", bookingDAO.getBookingListByOwner(user.getId()));
			return "listBookings";
		} else
			return "accessdenied";
	}
	
	@RequestMapping(value = "checkRoomToBeFree.html")
	public @ResponseBody String checkRoomToBeFreeJs(
			@RequestParam(value = "roomId", required = true) Integer roomId,
			@RequestParam(value = "dateIn", required = true) Date dateIn,
			@RequestParam(value = "dateOut", required = true) Date dateOut){
		String msg="occupied";
		if (checkRoomToBeFree(roomId, dateIn, dateOut))
			msg="free";
		return msg;
	}
	
	@RequestMapping(value = "calculateBookingCost.html")
	public @ResponseBody String calculateBookingCost(
			@RequestParam(value = "roomPrice", required = true) Integer roomPrice,
			@RequestParam(value = "dateIn", required = true) Date dateIn,
			@RequestParam(value = "dateOut", required = true) Date dateOut){
		String cost = ""+getBookingCost(dateIn,dateOut,roomPrice);
		return cost;
	}
	
	// Utils
	
	
	public Integer getBookingCost (Date dateIn, Date dateOut, Integer roomPrice){
		DateTime dateIN = new DateTime(dateIn);
		DateTime dateOUT = new DateTime(dateOut);
		int days =Days.daysBetween(dateIN, dateOUT).getDays();
		Integer cost = days*roomPrice;
		return cost;
	}
	
	/*Checks Room To Be Free of bookings
	 * safeCheck parameter determines whether room has confirmed booking or bookings at all.
	 */
	public boolean checkRoomToBeFree(Integer roomId, Date dateIn, Date dateOut){
				
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setRoomId(roomId);
		searchCriteria.setDateIn(dateIn);
		searchCriteria.setDateOut(dateOut);
		searchCriteria.setDateOut(dateOut);
		return bookingDAO.checkRoomBeforeBook(searchCriteria);
	}

	public boolean userIsAdminOrOwner(Integer bookId) {

		User user = userDAO.getLoggedUser();

		Booking booking = bookingDAO.getBookingById(bookId);
		if ((booking != null)
				&& ("ROLE_ADMIN".equals(user.getRole())
						|| booking.getOwner().getId().equals(user.getId())
						|| booking.getHotel().isOwner(user))) {
			return true;
		} else
			return false;
	}
}
