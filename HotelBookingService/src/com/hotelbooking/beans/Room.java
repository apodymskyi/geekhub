package com.hotelbooking.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "ROOM")
public class Room {

	@GeneratedValue
	@Id
	@Column(name="ID")
	private Integer id;

	@Column(name = "OCCUPANCY")
	private Integer occupancy;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "TYPE")
	private RoomType type;

	@Column(name = "PRICE")
	private int price;

	@Column(name = "NUMBER")
	private int roomNumber;
	

	@OneToMany(mappedBy="room", cascade=CascadeType.REMOVE, fetch=FetchType.LAZY)
	private List<Booking> bookings;
	
	@OneToMany(mappedBy="room", cascade=CascadeType.REMOVE, fetch=FetchType.LAZY)
	private List<Comment> comment;
	
	@ManyToOne
	@JoinColumn(name="HOTEL_ID")
	private Hotel hotel;
	
	

	// G/s section

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	public void setOccupancy(Integer occupancy) {
		this.occupancy = occupancy;
	}

	public int getOccupancy() {
		return occupancy;
	}

	public void setOccupancy(int occupancy) {
		this.occupancy = occupancy;
	}


	public RoomType getType() {
		return type;
	}

	public void setType(RoomType type) {
		this.type = type;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
		
	}
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}
