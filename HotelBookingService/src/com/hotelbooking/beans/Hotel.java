package com.hotelbooking.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "HOTEL")
public class Hotel {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Integer id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "CITY")
	private String city;

	@Column(name = "COUNTRY")
	private String country;

	@ManyToOne
	@JoinColumn(name = "OWNER_ID")
	private User owner;
	
	@OneToMany(mappedBy="hotel", cascade=CascadeType.REMOVE, fetch=FetchType.LAZY)
	private List<Room> rooms;
	
	@OneToMany(mappedBy="hotel", cascade=CascadeType.REMOVE, fetch=FetchType.LAZY)
	private List<Comment> comment;
	
	public boolean isOwner(User user){
		if(user.getId().equals(owner.getId()))
			return true;
		return false;
	}
	
	
//	G/s
	
	
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	
}
