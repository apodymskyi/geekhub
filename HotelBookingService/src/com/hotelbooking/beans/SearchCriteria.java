package com.hotelbooking.beans;

import java.util.Date;

public class SearchCriteria {
	
	private String country;
	private String city;
	private Integer hotelId;
	
	private Integer roomId;
	private RoomType roomType;
	private Integer roomTypeInt;
	private Integer roomOccupancy;
	private Integer roomPrice;
	
	private BookingStatus bookingStatus;
	
	private Date dateIn;
	private Date dateOut;
	
	private Integer limitResults;

	
	
	public Integer getRoomOccupancy() {
		return roomOccupancy;
	}

	public void setRoomOccupancy(Integer roomOccupancy) {
		this.roomOccupancy = roomOccupancy;
	}

	public Integer getRoomPrice() {
		return roomPrice;
	}

	public void setRoomPrice(Integer roomPrice) {
		this.roomPrice = roomPrice;
	}
		
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public RoomType getRoomType() {
		return roomType;
	}
	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}
	public Date getDateIn() {
		return dateIn;
	}
	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}
	public Date getDateOut() {
		return dateOut;
	}
	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}


	
	public Integer getHotelId() {
		return hotelId;
	}

	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public Integer getRoomTypeInt() {
		return roomTypeInt;
	}

	public void setRoomTypeInt(Integer roomTypeInt) {
		this.roomTypeInt = roomTypeInt;
	}

	public void setBookingStatus(BookingStatus bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public BookingStatus getBookingStatus() {
		return bookingStatus;
	}

	public Integer getLimitResults() {
		return limitResults;
	}

	public void setLimitResults(Integer limitResults) {
		this.limitResults = limitResults;
	}

		

}
