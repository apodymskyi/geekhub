package com.hotelbooking.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="BOOKING")
public class Booking {
	
	@Id
	@GeneratedValue
	@Column(name="ID")
	private Integer id;
	
	
	@Column(name="DATE_IN")
	private Date dateIn;
	
	@Column(name="DATE_OUT")
	private Date dateOut;
	
	@Column(name="STATUS")
	private BookingStatus status;
	
	@ManyToOne
	@JoinColumn(name="OWNER_ID")
	private User owner;
	
	@ManyToOne
	@JoinColumn(name="HOTEL_ID")
	private Hotel hotel;
	
//	(fetch = FetchType.EAGER)
	@ManyToOne
	@JoinColumn(name="ROOM_ID")
	private Room room;
	
	@OneToOne
	@JoinColumn(name="PAYMENT_ID")
	private Payment payment;
	
	public boolean isOwner(User user){
		if(user.getId().equals(owner.getId()))
			return true;
		return false;
	}
	
	
	
//	G/s section
	
	public Room getRoom() {
		return room;
	}
	public void setRoom(Room room) {
		this.room = room;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public BookingStatus getStatus() {
		return status;
	}
	public void setStatus(BookingStatus status) {
		this.status = status;
	}
	
	public Date getDateIn() {
		return dateIn;
	}
	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}
	public Date getDateOut() {
		return dateOut;
	}
	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}



	public Payment getPayment() {
		return payment;
	}



	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	
}
