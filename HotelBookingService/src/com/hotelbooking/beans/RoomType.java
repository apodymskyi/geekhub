package com.hotelbooking.beans;

public enum RoomType {
	LUX, HALF_LUX, STANDART, ECONOMY;
}
