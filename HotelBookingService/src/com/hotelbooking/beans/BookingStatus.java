package com.hotelbooking.beans;

public enum BookingStatus {
	NOT_CONFIRMED, CONFIRMED;
}
