
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class SubmitPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		ServletContext servletContext = request.getServletContext();
		
//		Testing here
		session.setAttribute("sessionA", "1");
		servletContext.setAttribute("appA", "1");
//		
		
		String action = request.getParameter("action");
		String scope = request.getParameter("scope");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		String message = request.getParameter("message");
		
		PrintWriter out = response.getWriter();
		response.setContentType("text/html; charset=utf-8"); 
		
		// Check scope
		 if ((scope == null)
				 ||!((scope.equals("session")) || (scope.equals("app")))) {
			 out.println("Bad scope parameter occured: "+scope);
			 return;
			}
				
		// Session action
		if (scope.equals("session")) {
			if ((action.equals("add"))||(action.equals("update"))) 
				session.setAttribute(name, value);
			else if (action.equals("remove"))
				session.removeAttribute(name);
			else  {
				out.println("Bad action parameter occured: "+action);
				return;
			}
		}
		
		else if (scope.equals("app")) {
			if ((action.equals("add"))||(action.equals("update"))) 
				servletContext.setAttribute(name, value);
			else if (action.equals("remove"))
				servletContext.removeAttribute(name);
			else {
				out.println("Bad action parameter occured: "+action);
				return;
			}
		}
		formResult(request,response);
		out.println("<br> Message: "+message);
	}
	
	public void formResult (HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
//		Forming answer
		
		HttpSession session = request.getSession();
		ServletContext servletContext = request.getServletContext();
		
		Enumeration<String> sessionValues = session.getAttributeNames();
		Enumeration<String> servletValues = servletContext.getAttributeNames();
		
		
		String key="";
		String val="";
		
		
		PrintWriter out = response.getWriter();
		response.setContentType("text/html; charset=utf-8"); 

		
//		Session answer
		out.println("Session key/val:");
		out.println("<hr>");
		out.println("<br>");
		out.println("<table>");
		
		while (sessionValues.hasMoreElements()){
			key=(String) sessionValues.nextElement();
			val=(String) session.getAttribute(key).toString();
			out.println("<tr><td>" + key + "</td><td>" + val +"</td></tr>");
		}
		out.println("</table>");
		
		
//		Servlet answer
		
		key="";
		val="";
		
		out.println("<br>");
		out.println("Servlet key/val:");
		out.println("<hr>");
		out.println("<br>");
		out.println("<table>");
		
		while (servletValues.hasMoreElements()){
			key=(String) servletValues.nextElement();
			val=(String) servletContext.getAttribute(key).toString();
			out.println("<tr><td>" + key+ "</td><td>"+ val+"</td></tr>");
		}
		out.println("</table>");
	}
}

