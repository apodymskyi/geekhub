<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table border="1">
    <tr>
        <th>Title</th>
        <th>Description</th>
        <th>Owner</th>
        <th>Status</th>
        <th>Priority</th>
        <th></th>
    </tr>
    <c:forEach items="${tickets}" var="ticket">
        <tr>
            <td>${ticket.title}</td>
            <td>${ticket.description}</td>
            <td>${ticket.owner.name}</td>
            <td>${ticket.status}</td>
            <td>${ticket.priority}</td>
            <td><a href="/L12.BugTracker/loadTicket.html?id=${ticket.id}">Edit</a></td>
        </tr>
    </c:forEach>
</table>
<a href="/L12.BugTracker/loadTicket.html">Create New Ticket</a>