package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Ticket;
import com.geekhub.services.TicketDao;
import com.geekhub.services.UserDao;


@Controller
public class TicketController {

	@Autowired TicketDao ticketDao;
	@Autowired UserDao userDao;
	
		
		
		@RequestMapping(value="listTickets.html")
		public String list(ModelMap map){
			map.put("tickets", ticketDao.getTicketList());
			map.put("users", userDao.getUserList());
			return "tickets";
		}
		
		@RequestMapping(value="loadTicket.html")
		public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map){
			Ticket ticket = id ==null ? new Ticket(): ticketDao.getTicketById(id);
			map.put("ticket", ticket);
			map.put("users", userDao.getUserList());
			return "ticket";
		}
		
		@RequestMapping(value="deleteTicket.html")
		public String delete(@RequestParam(value="id", required=true) Integer id){
			ticketDao.deleteTicket(id);
			return "redirect:listTickets.html";
			
		}
		
		@RequestMapping(value="saveTicket.html")
		public String save(Ticket ticket){
			
			ticketDao.saveTicket(ticket);
			return "redirect:listTickets.html";
		}
	}

