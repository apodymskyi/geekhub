package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.User;
import com.geekhub.services.UserDao;

@Repository
@Transactional
public class UserDaoImpl implements UserDao{
	
	@Autowired SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<User> getUserList() {
		return sessionFactory.getCurrentSession().createQuery("from User").list();
	}
	
	public User getUserById(Integer id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	public void deleteUser(Integer id) {
		User user = getUserById(id);
		if(null != user)
			sessionFactory.getCurrentSession().delete(user);
		
	}

	public void saveUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		
	}

}
