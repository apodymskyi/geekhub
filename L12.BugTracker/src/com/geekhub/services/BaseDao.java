package com.geekhub.services;

import java.util.List;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;



import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;

import com.geekhub.beans.Entity;
import com.geekhub.beans.Ticket;
import com.geekhub.beans.User;

public class BaseDao extends JdbcTemplate {

	public <T extends Entity> T get(Class<T> clazz, Integer id) {
		return queryForObject("SELECT * FROM " + clazz.getSimpleName()
				+ " WHERE id =?", new Object[] { id },
				new BeanPropertyRowMapper<T>(clazz));
	}

	public <T extends Entity> List<T> list(Class<T> clazz) {
		return query("SELECT * FROM " + clazz.getSimpleName(),
				new BeanPropertyRowMapper(clazz));
	}

	public <T extends Entity> boolean delete(Class<T> clazz, Integer id) {
		return update("DELETE FROM " + clazz.getSimpleName() + " WHERE id = ?",	id) > 0;
	}
	
	public void save(final User user){
		
		if (user.isNew()){
//			Insert
			
			
		PreparedStatementCreator psc = new PreparedStatementCreator() {
		
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException{
				PreparedStatement psc = con.prepareStatement("INSERT INTO User (login, password)");
				psc.setString(1, user.getLogin());
				psc.setString(2, user.getPassword());
				psc.setString(3, user.getName());
				return null;
			}			
		};
	}
	}

		
	

	public void save(Ticket ticket){
		
	}
}
